import nodeResolve from 'rollup-plugin-node-resolve';
import replace from 'rollup-plugin-replace';

module.exports = {
  plugins: [
    nodeResolve(),
    // @rollup/plugin-replace is used to replace process.env.NODE_ENV
    // statements in the Workbox libraries to match your current environment.
    // This changes whether logging is enabled ('development') or disabled ('production').
    replace({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'production'),
    }),
  ]
}
