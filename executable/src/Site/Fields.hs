module Site.Fields where

import Data.Time.Clock         (UTCTime (..), getCurrentTime)
import Data.Time.Calendar      (toGregorian)
import Data.Time.Format        (formatTime, parseTimeM)
import Data.Time.Locale.Compat (TimeLocale, defaultTimeLocale)
import Hakyll
import RIO
import RIO.Partial             (read)
import System.FilePath.Posix   (splitFileName)

baseCtx :: Context String
baseCtx = mconcat
  [ routeField "route"
  , urlField' "url"
  , generateCopyrightYearField "copyright"
  , defaultContext
  ]

pageCtx :: Context String
pageCtx = mconcat
  [ dateField "date" humanTimeFmt
  , dateField "date-machine" machineTimeFmt
  , dateField "year" yearTimeFmt
  , modificationTimeField' "date-modified" humanTimeFmt
  , modificationTimeField' "date-modified-machine" machineTimeFmt
  , modificationTimeField' "year-modified" yearTimeFmt
  , field "toc-content" toc
  , baseCtx
  ]
  where
    toc item = loadBody ((itemIdentifier item) { identifierVersion = Just "toc" })

postCtx :: Tags -> Context String
postCtx tags = mconcat
  [ constField "post" "true"
  , wordCountField "word-count"
  , readTimeField "read-time"
  , tagsField "tags" tags
  , pageCtx
  ]

humanTimeFmt, machineTimeFmt, yearTimeFmt :: String
humanTimeFmt = "%B %e, %Y"
machineTimeFmt = "%Y-%m-%d"
yearTimeFmt = "%Y"

-- Template function to return routed path of an identifier
routeField :: String -> Context a
routeField key = functionField key routeFn
  where
    routeFn [x] _ = getUrl $ fromString (dropWhile (== '/') x)
    routeFn _ _ = error "No ident given"

-- urlField which uses our special functions for the URL
urlField' :: String -> Context a
urlField' key = field key $ getUrl . itemIdentifier

-- Get URL of normal version of file
-- See http://blaenkdenum.com/posts/post-feed-in-hakyll/
getUrl :: Identifier -> Compiler String
getUrl ident = fmap (maybe (error errMsg) toUrl') . getRoute . setVersion Nothing $ ident
  where
    errMsg = toFilePath ident ++ " not found"

-- toUrl, but remove index.html from links, i.e., url is /page/ not /page/index.html
toUrl' :: FilePath -> String
toUrl' = removeIndexStr . toUrl
  where removeIndexStr url = case splitFileName url of
          (dir, "index.html") -> dir
          _                   -> url

data ExtraLink = ExtraLink
  { extraLinkUrl  :: String
  , extraLinkName :: String
  }

-- | Obtain extra links from a page in the default way: parse them from the
-- @extra-links@ metadata field.
getExtraLinks :: MonadMetadata m => Identifier -> m [ExtraLink]
getExtraLinks identifier = do
  metadata <- getMetadata identifier
  return $ maybe [] (map (uncurry ExtraLink . read)) $ lookupStringList "extra-links" metadata

extraLinkToItem :: ExtraLink -> Item ExtraLink
extraLinkToItem extra = Item (fromString $ extraLinkUrl extra) extra

extraLinkCtx :: Context ExtraLink
extraLinkCtx = mconcat
  [
    field "url" $ return . extraLinkUrl . itemBody
  , field "name" $ return . extraLinkName . itemBody
  ]

wordCountField :: String -> Context String
wordCountField key = field key $ \item -> do
  wordCount <- wordCountForItem item
  return $ show wordCount

-- TODO: run this and pass arg as param to context, to only calculate once for
-- both word count and read time
wordCountForItem :: Item String -> Compiler Int
wordCountForItem item = do
  plainBody <- loadBody ((itemIdentifier item) { identifierVersion = Just "plain" })
  return . length . words $ plainBody

readTimeField :: String -> Context String
readTimeField key = field key $ \item -> do
  wordCount <- wordCountForItem item
  return . flip (++) " min" . show $ wordCount `div` 200

modificationTimeField' :: String -> String -> Context String
modificationTimeField' key fmt = field key $ \item -> do
  mtime <- getItemModifiedUTC defaultTimeLocale $ itemIdentifier item
  return $ formatTime defaultTimeLocale fmt mtime

currentYearTimeField :: String -> Context String
currentYearTimeField key = field key $ \_ -> do
  year <- unsafeCompiler getUTCYear
  pure (show year)

-- TODO: after updating to time >= 1.11 can use these type synonyms
-- getUTCDate :: IO (Year, MonthOfYear, DayOfMonth)
getUTCDate :: IO (Integer, Int, Int)
getUTCDate = do
  now <- getCurrentTime
  pure (toGregorian $ utctDay now)

getUTCYear :: IO Integer
getUTCYear = do
  (year, _, _) <- getUTCDate
  pure year

-- getLocalDate :: IO (Integer, Int, Int)
-- getLocalDate = do
--   now <- getCurrentTime
--   timezone <- getCurrentTimeZone
--   let zonenow = utcToLocalTime timezone now
--   pure (toGregorian $ localDay zoneNow)

getItemModifiedUTC :: TimeLocale        -- ^ Output time locale
                   -> Identifier        -- ^ Input page
                   -> Compiler UTCTime  -- ^ Parsed UTCTime
getItemModifiedUTC locale id' = do
  fileModifiedTime <- getItemModificationTime id'

  metadata <- getMetadata id'
  let tryField k fmt = lookupString k metadata >>= parseTime' fmt

  return $ fromMaybe fileModifiedTime $ msum $
      [tryField "modified" fmt | fmt <- formats] ++
      [tryField "updated"  fmt | fmt <- formats]
  where
    parseTime' = parseTimeM True locale
    formats    =
        [ "%a, %d %b %Y %H:%M:%S %Z"
        , "%Y-%m-%dT%H:%M:%S%Z"
        , "%Y-%m-%d %H:%M:%S%Z"
        , "%Y-%m-%d"
        , "%B %e, %Y %l:%M %p"
        , "%B %e, %Y"
        , "%b %d, %Y"
        ]

generateCopyrightYearField :: String -> Context String
generateCopyrightYearField key = functionField key fn
  where
    fn [publishYear, modifiedYear] _ = pure $
      if publishYear == modifiedYear
        then publishYear
        else publishYear ++ "–" ++ modifiedYear
    fn [publishYear] _ = pure $ publishYear
    fn _ _ = do
      currentYear <- unsafeCompiler getUTCYear
      pure $ "2012" ++ "–" ++ (show currentYear)
