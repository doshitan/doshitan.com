module Site.Pandoc where

import           Hakyll
import           RIO
import qualified RIO.Text             as T
import           Skylighting          (SyntaxMap, defaultSyntaxMap)
import           Text.Pandoc          (Block (Header, DefinitionList), Inline (Span))
import qualified Text.Pandoc          as Pandoc
import           Text.Pandoc.Options  (HTMLMathMethod (PlainMath),
                                       WriterOptions, writerHTMLMathMethod,
                                       writerNumberSections, writerSectionDivs,
                                       writerSyntaxMap, writerTOCDepth,
                                       writerTableOfContents, writerTemplate)
import           Text.Pandoc.Shared   as PandocUtil
import           Text.Pandoc.SideNote (usingSideNotes)
import           Text.Pandoc.Walk     (walk)

writerOptions :: Maybe SyntaxMap -> WriterOptions
writerOptions mCustomSyntaxMap = defaultHakyllWriterOptions
  { writerSectionDivs = True
  , writerTableOfContents = True
  , writerSyntaxMap = maybe defaultSyntaxMap (<> defaultSyntaxMap) mCustomSyntaxMap
  }

mPandocCompiler :: Maybe SyntaxMap -> Compiler (Item String)
mPandocCompiler mCustomSyntaxMap =
  pandocCompilerWithTransform
    defaultHakyllReaderOptions
    (writerOptions mCustomSyntaxMap)
    (usingSideNotes . walk (bumpHeaders . addIdToDefinitionLists))

feedPandocCompiler :: Compiler (Item String)
feedPandocCompiler = pandocCompilerWith defaultHakyllReaderOptions writerOptions'
  where writerOptions' = defaultHakyllWriterOptions { writerHTMLMathMethod = PlainMath
                                                    }

tocPandocCompiler :: Compiler (Item String)
tocPandocCompiler = pandocCompilerWith defaultHakyllReaderOptions writerOptions'
  where writerOptions' = (writerOptions Nothing)
          { writerNumberSections = True
          , writerTemplate = Just (
              case runIdentity (Pandoc.compileTemplate [] "$toc$") of
                Left e -> error e
                Right tmpl -> tmpl
            )
          , writerTOCDepth = 2
          }

plainPandocCompiler :: Compiler (Item String)
plainPandocCompiler = getResourceBody >>= readPandoc >>= writePlain

writePlain :: Item Pandoc.Pandoc -> Compiler (Item String)
writePlain = traverse $ \pandoc ->
  case Pandoc.runPure (Pandoc.writePlain Pandoc.def pandoc) of
    Left err -> fail $ show err
    Right x -> return (T.unpack x)

-- | Map every @h1@ to an @h2@, @h2@ to @h3@, etc. like 'demoteHeaders', but
-- as a pandoc transformer
bumpHeaders :: Block -> Block
bumpHeaders (Header lvl attrs inlines) = Header (lvl + 1) attrs inlines
bumpHeaders x = x

addIdToDefinitionLists :: Block -> Block
addIdToDefinitionLists (DefinitionList ds) = DefinitionList $ map addIdToDefinition ds
addIdToDefinitionLists x = x

type Definition = ([Inline], [[Block]])

-- Ideally would add `id` directly on the `dt` element in HTML, but this is
-- easier.
addIdToDefinition :: Definition -> Definition
addIdToDefinition (term, x) = (addId term, x)
  where
    addId :: [Inline] -> [Inline]
    addId elms = [Span (mkId elms, [], []) elms]

    mkId = PandocUtil.inlineListToIdentifier Pandoc.emptyExtensions
