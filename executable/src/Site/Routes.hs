module Site.Routes where

import Hakyll
import RIO
import RIO.FilePath     (takeBaseName, takeDirectory, (</>))
import RIO.List.Partial (foldr1)

contentRoute :: Routes
contentRoute = composeRoutes' [ directorizeRoute
                              , gsubRoute "posts/" (const "")
                              , gsubRoute "pages/" (const "")
                              , gsubRoute "projects/" (const "")
                              , stripDate
                              ]

stripDate :: Routes
stripDate = gsubRoute "^[0-9]{4}-[0-9]{2}-[0-9]{2}-" (const "")

-- replace a foo/bar.md by foo/bar/index.html
-- this way the url looks like: foo/bar/ in most browsers
directorizeRoute :: Routes
directorizeRoute = customRoute createIndexRoute
  where
    createIndexRoute ident = dirPart </> "index.html"
                             where
                               p = toFilePath ident
                               basename = takeBaseName p
                               directory = takeDirectory p
                               dirPart = if basename == "index"
                                 then directory
                                 else directory </> basename

composeRoutes' :: [Routes] -> Routes
composeRoutes' = foldr1 composeRoutes
