module Site.Config where

import RIO
import Hakyll
import System.Environment (lookupEnv)

getHakyllConf :: IO Configuration
getHakyllConf = do
  destDir <- getConfig destinationDirectory "DEST_DIR"
  cacheDir <- getConfig storeDirectory "CACHE_DIR"
  tmpDir <- getConfig tmpDirectory "TMP_DIR"
  contentDir <- getConfig providerDirectory "CONTENT_DIR"
  return defaultConfiguration
    { destinationDirectory = destDir
    , storeDirectory = cacheDir
    , tmpDirectory = tmpDir
    , providerDirectory = contentDir
    }
  where
    getConfig f envStr = fromMaybe (f defaultConfiguration) <$> lookupEnv envStr

feedConf :: String -> FeedConfiguration
feedConf title = FeedConfiguration
  { feedTitle = "doshitan.com: " ++ title
  , feedDescription = "Personal site of Tanner Doshier"
  , feedAuthorName = "Tanner Doshier"
  , feedAuthorEmail = "hi@doshitan.com"
  , feedRoot = "https://doshitan.com"
  }

-- Pages which need special processing and are therefore excluded from the
-- general processing for other pages
specialPages :: [Identifier]
specialPages = wrapElems "pages/" "" ["index.html"]

-- Prepend and/or append content to each element in a list of strings
wrapElems :: String         -- What to prepend (goes before)
          -> String         -- What to append  (goes after)
          -> [String]       -- The list to wrap
          -> [Identifier]   -- The wrapped strings as identifiers
wrapElems b a = map (fromFilePath . \x -> b ++ x ++ a)
