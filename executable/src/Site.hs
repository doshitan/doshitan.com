import           Data.Aeson
import qualified Data.Aeson.KeyMap             as KM
import qualified Data.Text.IO                  as TIO
import           Hakyll
import           Hakyll.Core.Compiler.Internal (compilerAsk, compilerProvider)
import           Hakyll.Core.Provider          (resourceFilePath)
import           Network.HTTP.Types.Status
import           RIO
import           RIO.FilePath                  (takeBaseName, takeDirectory)
import qualified RIO.HashMap                   as HM
import           RIO.List                      (intercalate)
import           RIO.List.Partial              (head)
import qualified RIO.Map                       as M
import qualified RIO.Text                      as T

import           Codec.Binary.UTF8.Generic     (toString)
import           Skylighting                   (loadSyntaxesFromDir)
import           System.Environment            (getEnv)
import           System.IO.Error               (isDoesNotExistError)
import           System.Process                (callCommand, readProcess)

import           Utils                         (safeName)

import           Site.Config
import           Site.Fields
import           Site.Pandoc
import           Site.Routes

redirects :: [(Identifier, String)]
redirects =
  [ ("key.txt", "9963DB09FD2A2622CC46FF0320160260F4C18825.txt")
  , ("20160260F4C18825.txt", "9963DB09FD2A2622CC46FF0320160260F4C18825.txt")
  , ("F4C18825.txt", "9963DB09FD2A2622CC46FF0320160260F4C18825.txt")
  ]

allContentPattern :: Pattern
allContentPattern = ("posts/*" .||. "pages/**" .||. "projects/*") .&&. hasNoVersion .&&. complement "**.metadata"

main :: IO ()
main = do
  hakyllConf <- getHakyllConf

  customSyntaxMap <- either error return =<< loadSyntaxesFromDir ((providerDirectory hakyllConf) ++ "/assets/syntax")
  let contentPandocCompiler = mPandocCompiler (Just customSyntaxMap)

  hakyllWith hakyllConf $ do
    tags <- buildTags ("posts/*" .&&. hasNoVersion .&&. complement "**.metadata") (fromCapture "tags/*/index.html" . safeName)
    let postTagsCtx = postCtx tags

    version "redirects" $ createRedirects redirects

    match ("raw/**") $ do
      route $ composeRoutes' [ gsubRoute "raw/" (const "")
                             -- hakyll patterns don't like hidden directories
                             , gsubRoute "well-known" (const ".well-known")
                             ]
      compile copyFileCompiler

    match "assets/img/favicon.ico" $ do
      route $ gsubRoute "assets/img/" (const "")
      compile copyFileCompiler

    match "assets/img/*" $ do
      route   idRoute
      compile copyFileCompiler

    match "assets/font/*" $ do
      route   idRoute
      compile copyFileCompiler

    match "assets/elm/*.elm" $ compile getResourceBody

    elmDep <- makePatternDependency "assets/elm/**.elm"
    rulesExtraDependencies [elmDep] $ create ["assets/js/elm.js"] $
      -- we only use the minified version, so don't generally need this
      -- available, but can be useful to inspect the output on occasion
      -- route idRoute
      compile $ do
        elmFiles <- loadAll "assets/elm/*.elm" :: Compiler [Item String]
        elmFilePaths <- traverse (getProviderFilePath . itemIdentifier) elmFiles :: Compiler [FilePath]
        let contentDir = takeDirectory $ head elmFilePaths
        elmOutput <- unsafeCompiler $ do
          existingFile <- tryJust (guard . isDoesNotExistError) $ getEnv "SEARCH_APP_FILE"
          case existingFile of
            Right file -> T.unpack <$> TIO.readFile file
            Left _ ->
              withSystemTempFile "output.js" $ \fp h -> do
                let outputFile = "--output=" <> fp
                    args = unwords $ elmFilePaths <> ["--optimize", outputFile]
                    cmd = "cd " <> contentDir <> " && elm make" <> " " <> args
                callCommand cmd
                T.unpack <$> TIO.hGetContents h
        makeItem elmOutput

    -- https://guide.elm-lang.org/optimization/asset_size.html
    create ["assets/js/elm.min.js"] $
      -- this is included with main.js for the site, but maybe useful to inspect
      -- separately on occasion
      -- route idRoute
      compile $ do
        let uglifyCompressArg = "pure_funcs='F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9',pure_getters,keep_fargs=false,unsafe_comps,unsafe"
        loadBody "assets/js/elm.js"
          >>= unixFilter "uglifyjs" ["--compress", uglifyCompressArg]
          >>= unixFilter "uglifyjs" ["--mangle"]
          >>= makeItem

    create ["assets/js/lunr.js"] $
      compile $ do
        lunrContent <- unsafeCompiler $ do
          lunrFile <- getEnv "LUNR_FILE"
          T.unpack <$> TIO.readFile lunrFile
        makeItem lunrContent

    create ["assets/js/lunr.min.js"] $
      compile $
        loadBody "assets/js/lunr.js"
          >>= unixFilter "uglifyjs" ["--compress", "--mangle"]
          >>= makeItem

    match "assets/js/main.js" $ do
      route idRoute
      compile $ do
        -- TODO: maybe use hakyll versions for minified versions?
        lunrJs <- loadBody "assets/js/lunr.min.js"
        elmJs <- loadBody "assets/js/elm.min.js"
        mainJs <- itemBody <$> getResourceBody
        makeItem $ lunrJs <> elmJs <> mainJs

    match "assets/js/service-worker.js" $ do
      -- route idRoute
      compile $ do
        serviceWorkerJs <- itemBody <$> getResourceBody

        unixFilter "rollup" ["--format", "iife", "--config", "--bundleConfigAsCjs", "-"] serviceWorkerJs
          >>= makeItem

    create ["service-worker.js"] $ do
      route idRoute
      compile $
        loadBody "assets/js/service-worker.js"
          >>= unixFilter "uglifyjs" ["--compress", "--mangle"]
          >>= makeItem

    clayDep <- makePatternDependency "assets/clay/**.hs"
    rulesExtraDependencies [clayDep] $ create ["assets/css/clay.css"] $
      compile $ do
        clayOutput <- unsafeCompiler $ do
          existingFile <- tryJust (guard . isDoesNotExistError) $ getEnv "COMPILED_CLAY_FILE"
          case existingFile of
            Right file -> T.unpack <$> TIO.readFile file
            Left _ -> readProcess "cabal" ["new-run", "-v0", "doshitan-css"] ""
        makeItem clayOutput

    let cssFiles = "assets/css/*.css" .&&. complement "assets/css/main.css"
    match cssFiles $ compile getResourceBody

    cssDependency <- makePatternDependency cssFiles
    rulesExtraDependencies [cssDependency] $ create ["assets/css/main.css"] $ do
      route idRoute
      compile $ do
        rawCssFiles <- loadAll cssFiles
        let rawCss = concatMap (mappend "\n" . itemBody) rawCssFiles
        makeItem $ compressCss rawCss

    match ("pages/**" .&&. complement (fromList specialPages) .&&. complement "**.metadata") $ do
      route contentRoute
      compile $ contentPandocCompiler
        >>= loadAndApplyTemplate "templates/post-or-page.html" pageCtx
        >>= loadAndApplyTemplate "templates/default.html" pageCtx

    match ("posts/*" .&&. hasNoVersion .&&. complement "**.metadata") $ do
      route contentRoute
      compile $ contentPandocCompiler
        >>= loadAndApplyTemplate "templates/post-or-page.html" postTagsCtx
        >>= loadAndApplyTemplate "templates/default.html" postTagsCtx

      version "feed" $ compile feedPandocCompiler

    match ("projects/*" .&&. complement "**.metadata")$ do
      route contentRoute
      compile $ do
        ident <- getUnderlying
        metadata <- getMetadata ident
        let projectTagName = fromMaybe (takeBaseName $ toFilePath ident) (lookupString "tag-name" metadata <|> lookupString "title" metadata)
        let getPostsForTag tag = fromList $ fromMaybe [] $ M.lookup tag $ M.fromList (tagsMap tags)
        projectTaggedPosts <- fmap (take 5) $ recentFirst =<< loadAll (getPostsForTag projectTagName)
        extraLinks <- map extraLinkToItem <$> getExtraLinks ident
        let projectPostFields = if null projectTaggedPosts
                                  then mempty
                                  else mconcat
                                          [
                                            listField "posts" postTagsCtx (return projectTaggedPosts)
                                          , field "all-posts-url" (return . return . (++) "/" . toFilePath $ tagsMakeId tags projectTagName)
                                          ]
        let projectCtx = mconcat
              [
                listField "extra-links" extraLinkCtx (return extraLinks)
              , projectPostFields
              , pageCtx
              ]

        contentPandocCompiler
          >>= loadAndApplyTemplate "templates/project.html" projectCtx
          >>= loadAndApplyTemplate "templates/default.html" baseCtx

    match allContentPattern $ do
      version "toc" $ compile tocPandocCompiler

      version "plain" $ compile plainPandocCompiler

      version "json-search" $
        compile $ do
          ident <- getUnderlying
          metadata <- getMetadata ident
          plainBody <- loadBody (ident { identifierVersion = Just "plain" }) :: Compiler String
          url <- getUrl ident
          let item = KM.insert "id" (toJSON url) $ KM.insert "body" (toJSON plainBody) metadata
          makeItem $ renderToJSON item

    create ["search/all-content.json"] $
      compile $ do
        allJsonItems <- loadAll $ fromVersion (Just "json-search")
        let allJson = fmap itemBody allJsonItems
        makeItem $ "[" <> (intercalate "," allJson) <> "]"

    create ["search/index.json"] $ do
      route idRoute
      compile $
        loadBody "search/all-content.json"
          >>= unixFilter "build-search-index" []
          >>= makeItem

    create ["search/data.json"] $ do
      route idRoute
      compile $ do
        allItems <- loadAll allContentPattern :: Compiler [Item String]
        let allItemIds = fmap itemIdentifier allItems
        allItemsData <- forM allItemIds $
          \ident -> do
            title <- getMetadataField' ident "title"
            url <- getUrl ident
            return (url, HM.fromList [("title" :: String, title)])
        makeItem $ renderToJSON (HM.fromList allItemsData)

    create ["log.html"] $ do
      route directorizeRoute
      listPosts "Log" "All posts on doshitan.com" "posts/*" tags

    match "pages/index.html" $ do
      route   $ gsubRoute "pages/" (const "")
      compile $ do
        posts <- fmap (take 5) . recentFirst =<< loadAll ("posts/*" .&&. hasNoVersion)
        let indexCtx = mconcat
              [ listField "posts" postTagsCtx (return posts)
              , tagCloudField "tag-cloud" 85 165 tags
              , baseCtx
              ]

        getResourceBody
          >>= applyAsTemplate indexCtx
          >>= loadAndApplyTemplate "templates/default.html" indexCtx

    match "templates/*" $ compile templateCompiler

    tagsRules tags $ \tag pattern' -> do
      let title = "Posts tagged " ++ tag
      let description = "Posts tagged " ++ tag
      route idRoute
      listPosts title description pattern' tags

      -- Create Atom feed for each tag
      version "atom" $ do
        route $ setExtension "xml"
        compileFeed title pattern' tags

    create ["atom.xml"] $ do
      route idRoute
      compileFeed "All posts" "posts/*" tags

    -- custom error pages
    for_ [ (badRequest400, Nothing)
         , (forbidden403, Nothing)
         , (notFound404, Just "Oops, could not find that page. Maybe try the search to find what you were looking for.")
         , (methodNotAllowed405, Nothing)
         , (status500, Nothing)
         ] $ \(status, mMsg) -> do
      create [fromString (show (statusCode status) ++ ".html")] $ do
        route idRoute
        compile $ do
          let ctx = mconcat [ constField "code" (show $ statusCode status)
                            , constField "status-msg" (T.unpack . T.decodeUtf8With T.lenientDecode $ statusMessage status)
                            , foldMap (constField "body") mMsg
                            , baseCtx
                            ]
          makeItem ""
            >>= loadAndApplyTemplate "templates/error.html" ctx
            >>= loadAndApplyTemplate "templates/default.html" baseCtx

    create ["offline.html"] $ do
      route idRoute
      compile $ do
        let ctx = mconcat [ constField "status-msg" "Offline"
                          , constField "body" "You appear to be offline and the content you are trying to reach was not cached for offline access."
                          , baseCtx
                          ]
        makeItem ""
          >>= loadAndApplyTemplate "templates/error.html" ctx
          >>= loadAndApplyTemplate "templates/default.html" baseCtx

--------------------------------------------------------------------------------
listPosts :: String -> String -> Pattern -> Tags -> Rules ()
listPosts title description pattern' tags = compile $ do
      list <- recentFirst =<< loadAll (pattern' .&&. hasNoVersion)
      let ctx = mconcat [ constField "title" title
                        , constField "meta-description" description
                        , listField "posts" (postCtx tags) (return list)
                        , baseCtx
                        ]

      makeItem ""
        >>= loadAndApplyTemplate "templates/post-list.html" ctx
        >>= loadAndApplyTemplate "templates/page-title.html" ctx
        >>= loadAndApplyTemplate "templates/default.html" ctx


compileFeed :: String -> Pattern -> Tags -> Rules ()
compileFeed title pattern' tags = compile $ do
    let feedCtx = postCtx tags `mappend` bodyField "description"
    feedPattern <- getFeedVersionPattern pattern'
    loadAll feedPattern
      >>= fmap (take 10) . recentFirst
      >>= renderAtom (feedConf title) feedCtx


getFeedVersionPattern :: Pattern -> Compiler Pattern
getFeedVersionPattern pattern'= do
  normalItems <- loadAll pattern' :: Compiler [Item String]
  return $ fromList (map feedId normalItems)
  where
    feedId :: Item a -> Identifier
    feedId = setVersion (Just "feed") . itemIdentifier


-- | Get the file path of a resource
getProviderFilePath :: Identifier -> Compiler FilePath
getProviderFilePath id' = do
    provider <- compilerProvider <$> compilerAsk
    return $ resourceFilePath provider id'

-- from https://github.com/beerendlauwers/hakyll-extra/blob/master/src/Hakyll/Core/Util/JSON.hs
-- | Produces a String that is valid JSON (can be copy-pasted into a browser and parsed).
renderToJSON :: ToJSON a => a -> String
renderToJSON = toString . encode
