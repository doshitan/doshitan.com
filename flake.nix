{
  description = "doshitan.com";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        overlay = import ./nix/overlay.nix { };
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ overlay ];
        };
      in rec {
        ciInputs = [
          pkgs.awscli
          pkgs.bash
          pkgs.coreutils
          pkgs.gnumake
          pkgs.linkchecker
          pkgs.parallel
          pkgs.s3cmd
        ];

        contentBuildInputs = [
          pkgs.gnumake
          pkgs.elmPackages.elm
          pkgs.nodePackages.uglify-js
          pkgs.nodePackages.rollup
          pkgs.nodePackages."@rollup/rollup-linux-x64-gnu"
          pkgs.nodejs
          packages.build-search-index
        ];

        contentLunrFile =
          "${pkgs.contentNodeRuntimeEnv}/lib/node_modules/lunr/lunr.js";

        # rollup doesn't support using $NODE_PATH, so need to create a
        # "node_modules" folder it can see in order for it to find stuff
        #
        # https://github.com/rollup/rollup/issues/4271
        linkNodeModulesStatement = ''
          ln -sf "${pkgs.contentNodeRuntimeEnv}/lib/node_modules"
        '';

        packages = utils.lib.flattenTree rec {
          executable =
            pkgs.haskellPackages.callCabal2nix "doshitan-site" ./executable { };
          doshitan-css-exec =
            (pkgs.callPackage ./content/assets/clay { pkgs = pkgs; }).css-exec;
          doshitan-css =
            (pkgs.callPackage ./content/assets/clay { pkgs = pkgs; }).css;

          content = pkgs.stdenv.mkDerivation rec {
            name = "doshitan-site-content-${version}";
            version = "0.0.0";
            srcs = [
              (./scripts/version-assets.sh)
              (./content)
              (./rollup.config.js)
            ];
            unpackPhase = ''
              cp $(echo $srcs | cut -d ' ' -f 1) ./version-assets.sh
              cp -a $(echo $srcs | cut -d ' ' -f 2)/. content
              cp $(echo $srcs | cut -d ' ' -f 3) ./rollup.config.js
            '';
            phases = [ "unpackPhase" "buildPhase" "checkPhase" ];
            buildInputs = contentBuildInputs ++ [ executable search-app ];

            doCheck = true;

            LOCALE_ARCHIVE = pkgs.lib.optionalString pkgs.stdenv.isLinux
              "${pkgs.glibcLocales}/lib/locale/locale-archive";
            LANG = "en_US.UTF-8";
            LC_TYPE = "en_US.UTF-8";

            SITE_CMD = "${executable}/bin/site";

            buildPhase = ''
              export DEST_DIR=$out
              export CONTENT_DIR=./content
              export LUNR_FILE=${contentLunrFile}
              export SEARCH_APP_FILE=${search-app}/SearchMain.js
              export COMPILED_CLAY_FILE=${doshitan-css}

              ${linkNodeModulesStatement}

              $SITE_CMD rebuild

              patchShebangs ./version-assets.sh
              ./version-assets.sh
            '';

            checkPhase = ''
              export DEST_DIR=$out
              $SITE_CMD check --internal-links
            '';
          };

          build-search-index = pkgs.stdenv.mkDerivation rec {
            name = "build-search-index-${version}";
            version = "0.0.0";

            src = ./scripts/build-search-index.js;

            buildInputs = [ pkgs.makeWrapper pkgs.nodejs ];

            phases = [ "installPhase" "fixupPhase" ];

            installPhase = ''
              mkdir -p $out/bin
              cat >$out/bin/build-search-index <<EOF
                #!${pkgs.stdenv.shell}/bin/sh
                ${pkgs.nodejs}/bin/node ${src}
              EOF
            '';

            postFixup = ''
              chmod +x $out/bin/build-search-index
              wrapProgram $out/bin/build-search-index \
                --set NODE_PATH "${pkgs.contentNodeRuntimeEnv}/lib/node_modules"
            '';
          };

          search-app = pkgs.callPackage ./content/assets/elm { pkgs = pkgs; };

          merriweather = pkgs.stdenv.mkDerivation rec {
            name = "doshitan-merriweather-${version}";
            version = "0.0.0";
            src = ./scripts/merriweather.sh;
            phases = [ "unpackPhase" "buildPhase" ];
            unpackPhase = ''
              cp $src ./merriweather.sh
            '';
            buildInputs = [
              pkgs.merriweather
              pkgs.python3Packages.fonttools
              pkgs.python3Packages.brotlipy
            ];
            buildPhase = ''
              substituteInPlace merriweather.sh --replace "/usr/bin/env" "${pkgs.coreutils}/bin/env"

              export MERRIWEATHER_SRC_DIR=${pkgs.merriweather}
              export MERRIWEATHER_DEST_DIR=$out
              ./merriweather.sh
              echo "now run: make merriweather-save MERRIWEATHER_DEST_DIR=result"
            '';
          };

          andada-pro = pkgs.stdenv.mkDerivation rec {
            name = "doshitan-andada-pro-${version}";
            version = "0.0.0";
            src = ./scripts/andada-pro.sh;
            phases = [ "unpackPhase" "buildPhase" ];
            unpackPhase = ''
              cp $src ./andada-pro.sh
            '';
            buildInputs = [
              pkgs.andada-pro
              pkgs.python3Packages.fonttools
              pkgs.python3Packages.brotlipy
            ];
            buildPhase = ''
              substituteInPlace andada-pro.sh --replace "/usr/bin/env" "${pkgs.coreutils}/bin/env"

              export ANDADA_SRC_DIR=${pkgs.andada-pro}
              export ANDADA_DEST_DIR=$out
              ./andada-pro.sh
              echo "now run: make andada-pro-save ANDADA_DEST_DIR=result"
            '';
          };

          firaMono = pkgs.stdenv.mkDerivation rec {
            name = "doshitan-firaMono-${version}";
            version = "0.0.0";
            src = ./scripts/fira-mono.sh;
            phases = [ "unpackPhase" "buildPhase" ];
            unpackPhase = ''
              cp $src ./fira-mono.sh
            '';
            buildInputs = [
              pkgs.fira-mono
              pkgs.python3Packages.fonttools
              pkgs.python3Packages.brotlipy
            ];
            buildPhase = ''
              substituteInPlace fira-mono.sh --replace "/usr/bin/env" "${pkgs.coreutils}/bin/env"

              export FIRA_MONO_SRC_DIR=${pkgs.fira-mono}/share/fonts/opentype/
              export FIRA_MONO_DEST_DIR=$out
              ./fira-mono.sh
              echo "now run: make fira-mono-save FIRA_MONO_DEST_DIR=result"
            '';
          };

          contentAndDeployScript = pkgs.stdenv.mkDerivation {
            name = "content-and-deploy-script";

            nativeBuildInputs = ciInputs ++ [ pkgs.makeWrapper ];
            src = ./makefile;

            phases = [ "installPhase" "fixupPhase" ];

            installPhase = ''
              mkdir -p $out/content

              cp -a ${packages.content}/. $out/content

              cp $src $out/makefile

              makeWrapper ${ciDeployScript}/bin/deploy $out/deploy \
                --prefix PATH ":" \
                      "${pkgs.lib.makeBinPath ciInputs}" \
                --set DEST_DIR $out/content
            '';
          };

          ciDeployScript = pkgs.writeScriptBin "deploy" ''
            #!${pkgs.runtimeShell}

            make sync content-check-all
          '';
        };

        defaultPackage = packages.content;

        devShell = with pkgs;
          haskellPackages.shellFor {
            packages = p: [ packages.executable packages.doshitan-css-exec ];
            LUNR_FILE = contentLunrFile;
            buildInputs = builtins.concatLists [
              # for haskell development
              (with haskellPackages; [ cabal-install ])

              # for infra/
              [
                pkgs.opentofu
              ]

              # for font make targets
              [
                pkgs.unzip
                pkgs.curl
              ]

              # for icon generation script
              [
                pkgs.graphicsmagick
                pkgs.optipng
                pkgs.libwebp
              ]

              # misc. tools
              [
                nixfmt-rfc-style
                yq-go
              ]

              # for local deploys
              ciInputs

              # for elm stuff mostly
              contentBuildInputs
              [ pkgs.elmPackages.elm-format pkgs.elm2nix ]
            ];

            shellHook = ''
              ${linkNodeModulesStatement}
            '';
          };

        checks = with packages; {
          inherit content contentAndDeployScript executable;
        };
      });
}
