#!/usr/bin/env bash

: "${FIRA_MONO_SRC_DIR:=./.fira-mono/fonts/otf}"
: "${FIRA_MONO_DEST_DIR:=./.fira-mono/subset_fonts}"
DEST_FONT_DIR=$FIRA_MONO_DEST_DIR/font

: "${FIRA_MONO_FONT_VERS:=Regular Bold}"

BASIC_LATIN_NO_CONTROL_CHARACTERS="U+0020-007E"
BOX_CHARACTERS="U+2500-257F"

: "${FIRA_MONO_SUBSET_RANGE:=$BASIC_LATIN_NO_CONTROL_CHARACTERS,$BOX_CHARACTERS}"

LAYOUT_FEATURES_ARGS=("--layout-features=''")

function srcFileForVer {
  echo $FIRA_MONO_SRC_DIR/FiraMono-$FONTVER.otf
}

function destFilenameForVer {
  local fileExt=$1
  echo FiraMono-$FONTVER.subset.$fileExt
}

function fontStyleForVer {
  case $FONTVER in
    *) echo "normal" ;;
  esac
}

function fontWeightForVer {
  case $FONTVER in
    Bold) echo "700" ;;
    *) echo "400" ;;
  esac
}

mkdir -p $DEST_FONT_DIR

# cleanup any existing version of the css
rm -f $FIRA_MONO_DEST_DIR/fira-mono.css

for FONTVER in $FIRA_MONO_FONT_VERS ; do
  echo "processing $FONTVER"
  pyftsubset $(srcFileForVer) --output-file=$DEST_FONT_DIR/$(destFilenameForVer "otf") "${LAYOUT_FEATURES_ARGS[@]}" --unicodes=$FIRA_MONO_SUBSET_RANGE
  pyftsubset $(srcFileForVer) --output-file=$DEST_FONT_DIR/$(destFilenameForVer "woff") --flavor=woff "${LAYOUT_FEATURES_ARGS[@]}" --unicodes=$FIRA_MONO_SUBSET_RANGE
  pyftsubset $(srcFileForVer) --output-file=$DEST_FONT_DIR/$(destFilenameForVer "woff2") --flavor=woff2 "${LAYOUT_FEATURES_ARGS[@]}" --unicodes=$FIRA_MONO_SUBSET_RANGE

  echo "writing css for $FONTVER"
  tee -a $FIRA_MONO_DEST_DIR/fira-mono.css > /dev/null <<EOF
@font-face {
  font-family: "Fira Mono";
  font-style: $(fontStyleForVer);
  font-weight: $(fontWeightForVer);
  font-display: swap;
  src: url(/assets/font/$(destFilenameForVer "woff2")) format("woff2"),
       url(/assets/font/$(destFilenameForVer "woff")) format("woff"),
       url(/assets/font/$(destFilenameForVer "otf")) format("opentype");
  unicode-range: $FIRA_MONO_SUBSET_RANGE;
}
EOF
done
