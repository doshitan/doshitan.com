#!/usr/bin/env sh

set -eux

nix build -vv -L .#contentAndDeployScript
./result/deploy
