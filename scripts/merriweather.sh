#!/usr/bin/env bash

: "${MERRIWEATHER_SRC_DIR:=./.merriweather/fonts}"
: "${MERRIWEATHER_DEST_DIR:=./.merriweather/subset_fonts}"
DEST_FONT_DIR=$MERRIWEATHER_DEST_DIR/font

MERRIWEATHER_FONT_VERS=("Regular" "Italic" "Bold")

BASIC_LATIN_SUBSET="U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD"
# ENGLISH_ONLY_SUBSET="U+0000-00A0,U+00A2-00A9,U+00AC-00AE,U+00B0-00B7,U+00B9-00BA,U+00BC-00BE,U+00D7,U+00F7,U+2000-206F,U+2074,U+20AC,U+2122,U+2190-21BB,U+2212,U+2215,U+F8FF,U+FEFF,U+FFFD"
# Merriweather has poor coverage of Greek characters, will have to rely on fallbacks
# GREEK_LOWER="U+03B1-03C9"
# GREEK_UPPER="U+0391-03A9"
# GREEK_SUBSET="$GREEK_LOWER,$GREEK_UPPER"
# ANGLE_QUOTES="U+00AB,U+00BB"
# BASIC_ACCENTED_CHARS="U+00C0-00FF,U+152-153,U+178"
BASIC_ARROWS="U+2190-21BB"
# MISC_MATH_PHYSICS="U+2202-2265,U+27E8-27E9"
MISC="U+25FC"
# : "${MERRIWEATHER_SUBSET_RANGE:=U+01A0-007E,U+2018-2026}"
# : "${MERRIWEATHER_SUBSET_RANGE:=U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}"
# : "${MERRIWEATHER_SUBSET_RANGE:=$ENGLISH_ONLY_SUBSET,$ANGLE_QUOTES,$MISC}"
: "${MERRIWEATHER_SUBSET_RANGE:=$BASIC_LATIN_SUBSET,$BASIC_ARROWS,$MISC}"

LAYOUT_FEATURES_ARGS=("--layout-features+=case,frac,onum,pnum,lnum,tnum,subs,sups,zero,smcp,c2sc")

function srcFileForVer {
  # The 6pt is supposed to be the closest to the original font:
  # https://github.com/SorkinType/Merriweather/issues/523#issuecomment-752799372
  echo "$MERRIWEATHER_SRC_DIR/ttfs/Merriweather-6pt$FONTVER.ttf"
}

function destFilenameForVer {
  local fileExt=$1
  echo Merriweather$FONTVER.subset.$fileExt
}

function fontStyleForVer {
  case $FONTVER in
    Italic) echo "italic" ;;
    *) echo "normal" ;;
  esac
}

function fontWeightForVer {
  case $FONTVER in
    Bold) echo "700" ;;
    *) echo "400" ;;
  esac
}

mkdir -p $DEST_FONT_DIR

# cleanup any existing version of the css
rm -f $MERRIWEATHER_DEST_DIR/merriweather.css

for FONTVER in "${MERRIWEATHER_FONT_VERS[@]}"; do
  echo "processing $FONTVER: $(srcFileForVer)"
  pyftsubset $(srcFileForVer) --output-file=$DEST_FONT_DIR/$(destFilenameForVer "otf") "${LAYOUT_FEATURES_ARGS[@]}" --unicodes=$MERRIWEATHER_SUBSET_RANGE
  pyftsubset $(srcFileForVer) --output-file=$DEST_FONT_DIR/$(destFilenameForVer "woff") --flavor=woff "${LAYOUT_FEATURES_ARGS[@]}" --unicodes=$MERRIWEATHER_SUBSET_RANGE
  pyftsubset $(srcFileForVer) --output-file=$DEST_FONT_DIR/$(destFilenameForVer "woff2") --flavor=woff2 "${LAYOUT_FEATURES_ARGS[@]}" --unicodes=$MERRIWEATHER_SUBSET_RANGE

  echo "writing css for $FONTVER"
  tee -a $MERRIWEATHER_DEST_DIR/merriweather.css > /dev/null <<EOF
@font-face {
  font-family: "Merriweather";
  font-style: $(fontStyleForVer);
  font-weight: $(fontWeightForVer);
  font-display: swap;
  src: url(/assets/font/$(destFilenameForVer "woff2")) format("woff2"),
       url(/assets/font/$(destFilenameForVer "woff")) format("woff"),
       url(/assets/font/$(destFilenameForVer "otf")) format("opentype");
  unicode-range: $MERRIWEATHER_SUBSET_RANGE;
}
EOF
done
