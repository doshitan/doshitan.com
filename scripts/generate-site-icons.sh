#!/usr/bin/env bash
set -euo pipefail

SRC_FILE=$1
DEST=$2

mkdir -p "$DEST"

function resize_square() {
    local dimension=$1
    local size_str="${dimension}x${dimension}"
    gm convert -size "$size_str" "$SRC_FILE" -resize "$size_str^" -gravity center -extent "$size_str" -strip -
}

favicon_sizes="16 32"
icon_sizes="120 152 167 180 192 512"

html_header_links=()
manifest_entries=()

for s in $favicon_sizes; do
    size_spec="$s"x"$s"
    file_name=favicon-$size_spec
    src_png=$file_name.png
    resize_square "$s" > "$DEST/$src_png"
    optipng -o7 "$DEST/$src_png" -dir "$DEST/optipng"

    # these are so small, lossy encoding is fine
    mkdir -p "$DEST/webp"
    gm convert "$DEST/$src_png" -define webp:method=6 "$DEST/webp/$file_name.webp"

    # want the WEBP versions listed after the PNG ones, that seems to get at
    # least Firefox and Chromium to use them over the PNGs
    html_header_links+=($"<link rel=\"icon\" type=\"image/png\" href=\"/assets/img/$file_name.png\" sizes=\"$size_spec\">")
    html_header_links+=($"<link rel=\"icon\" type=\"image/webp\" href=\"/assets/img/$file_name.webp\" sizes=\"$size_spec\">")
done

for s in $icon_sizes; do
    size_spec="$s"x"$s"
    file_name=icon-$size_spec
    src_png=$file_name.png
    resize_square "$s" > "$DEST/$src_png"
    optipng -o7 "$DEST/$src_png" -dir "$DEST/optipng"


    # these are bigger and look noticeably worse even with highest quality lossy
    # encoding, may be able to get over that for myself at some point, but the
    # "near lossless" still cuts file sizes in half without any real loss in
    # detail so rolling with that for now
    mkdir -p "$DEST/webp-near-lossless-60"
    cwebp -near_lossless 60 "$DEST/$src_png" -o "$DEST/webp-near-lossless-60/$file_name.webp"

    # want the WEBP version listed first in both places to have them preferred by browsers
    html_header_links+=($"<link rel=\"apple-touch-icon-precomposed\" type=\"image/webp\" href=\"/assets/img/$file_name.webp\" sizes=\"$s""x"$"$s\">")
    html_header_links+=($"<link rel=\"apple-touch-icon-precomposed\" type=\"image/png\" href=\"/assets/img/$file_name.png\" sizes=\"$s""x"$"$s\">")
    manifest_entries+=($"{\"src\": \"/assets/img/$file_name.webp\", \"sizes\": \"$size_spec\", \"type\": \"image/webp\"}")
    manifest_entries+=($"{\"src\": \"/assets/img/$file_name.png\", \"sizes\": \"$size_spec\", \"type\": \"image/png\"}")
done

echo ""
echo "HTML Header Links"
for line in "${html_header_links[@]}"; do
     echo "$line"
done

echo ""
echo "Web Manifest Entries"
for entry in "${manifest_entries[@]}"; do
     echo "$entry,"
done
