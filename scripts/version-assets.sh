#!/usr/bin/env bash
set -euo pipefail

# The order of the different types has some impact for version-assets, we want
# to rename font files before hashing the CSS, because the CSS includes
# references to the font files.
VERSION_ASSET_ORDER='*/font/* */css/* */js/*'

# would probably prefer `realpath --relative-to=$(DEST_DIR) {} \+`, but that's not happy inside docker
ASSETS=$(for pattern in $VERSION_ASSET_ORDER; do find $DEST_DIR -type f -path "$pattern" -exec sh -c "echo {} | sed -r 's|$DEST_DIR/?||'" \;; done)

for ASSET in $ASSETS ; do
    HASH=$(md5sum $DEST_DIR/$ASSET | cut -c 1-5 ) ;
    NEW_NAME=$(dirname $ASSET)/$HASH.$( echo $ASSET | awk -F . '{print $NF}' ) ;
    sed -ri "s|$(echo $ASSET)\b|$(echo $NEW_NAME)|g" $(find $DEST_DIR/ -type f -iname '*.html' -o -iname '*.css' -o -iname '*.js') ;
    mv $DEST_DIR/$(echo $ASSET) $DEST_DIR/$(echo $NEW_NAME) ;
done
