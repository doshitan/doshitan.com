#!/usr/bin/env bash

: "${ANDADA_SRC_DIR:=./.andada/fonts}"
: "${ANDADA_DEST_DIR:=./.andada/subset_fonts}"
DEST_FONT_DIR=$ANDADA_DEST_DIR/font

ANDADA_FONT_VERS=("Regular" "Italic" "Bold")

# BASIC_LATIN_SUBSET="U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD"
ENGLISH_ONLY_SUBSET="U+0000-00A0,U+00A2-00A9,U+00AC-00AE,U+00B0-00B7,U+00B9-00BA,U+00BC-00BE,U+00D7,U+00F7,U+2000-206F,U+2074,U+20AC,U+2122,U+2190-21BB,U+2212,U+2215,U+F8FF,U+FEFF,U+FFFD"
# GREEK_LOWER="U+03B1-03C9"
# GREEK_UPPER="U+0391-03A9"
# GREEK_SUBSET="$GREEK_LOWER,$GREEK_UPPER"
# ANGLE_QUOTES="U+00AB,U+00BB"
BASIC_ACCENTED_CHARS="U+00C0-00FF,U+152-153,U+178"
BASIC_ARROWS="U+2190-21BB"
# MISC_MATH_PHYSICS="U+2202-2265,U+27E8-27E9"
MISC="U+25FC"
# : "${ANDADA_SUBSET_RANGE:=U+01A0-007E,U+2018-2026}"
# : "${ANDADA_SUBSET_RANGE:=U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}"
# : "${ANDADA_SUBSET_RANGE:=$ENGLISH_ONLY_SUBSET,$ANGLE_QUOTES,$MISC}"
# : "${ANDADA_SUBSET_RANGE:=$BASIC_LATIN_SUBSET,$BASIC_ARROWS,$MISC}"
: "${ANDADA_SUBSET_RANGE:=$ENGLISH_ONLY_SUBSET,$BASIC_ACCENTED_CHARS,$BASIC_ARROWS,$MISC}"

# LAYOUT_FEATURES_ARGS=("--layout-features+=case,frac,onum,pnum,lnum,tnum,subs,sups,zero,smcp,c2sc")
# LAYOUT_FEATURES_ARGS=("--layout-features+=case,frac,onum,pnum,lnum,tnum,subs,sups,smcp,c2sc,ordn" "--layout-features-=calt,ccmp")
LAYOUT_FEATURES_ARGS=("--layout-features=case,liga,locl,lnum,onum,ordn,tnum,kern,rvrn,smcp,c2sc")

function srcFileForVer {
  echo "$ANDADA_SRC_DIR/otf/AndadaPro-$FONTVER.otf"
}

function destFilenameForVer {
  local fileExt=$1
  echo AndadaPro-$FONTVER.subset.$fileExt
}

function fontStyleForVer {
  case $FONTVER in
    Italic) echo "italic" ;;
    *) echo "normal" ;;
  esac
}

function fontWeightForVer {
  case $FONTVER in
    Bold) echo "700" ;;
    *) echo "400" ;;
  esac
}

mkdir -p $DEST_FONT_DIR

# cleanup any existing version of the css
rm -f $ANDADA_DEST_DIR/andada.css

for FONTVER in "${ANDADA_FONT_VERS[@]}"; do
  echo "processing $FONTVER: $(srcFileForVer)"
  pyftsubset $(srcFileForVer) --output-file=$DEST_FONT_DIR/$(destFilenameForVer "otf") "${LAYOUT_FEATURES_ARGS[@]}" --unicodes=$ANDADA_SUBSET_RANGE
  pyftsubset $(srcFileForVer) --output-file=$DEST_FONT_DIR/$(destFilenameForVer "woff") --flavor=woff "${LAYOUT_FEATURES_ARGS[@]}" --unicodes=$ANDADA_SUBSET_RANGE
  pyftsubset $(srcFileForVer) --output-file=$DEST_FONT_DIR/$(destFilenameForVer "woff2") --flavor=woff2 "${LAYOUT_FEATURES_ARGS[@]}" --unicodes=$ANDADA_SUBSET_RANGE

  echo "writing css for $FONTVER"
  tee -a $ANDADA_DEST_DIR/andada-pro.css > /dev/null <<EOF
@font-face {
  font-family: "Andada Pro";
  font-style: $(fontStyleForVer);
  font-weight: $(fontWeightForVer);
  font-display: swap;
  src: url(/assets/font/$(destFilenameForVer "woff2")) format("woff2"),
       url(/assets/font/$(destFilenameForVer "woff")) format("woff"),
       url(/assets/font/$(destFilenameForVer "otf")) format("opentype");
  unicode-range: $ANDADA_SUBSET_RANGE;
}
EOF
done
