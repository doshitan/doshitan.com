#!/usr/bin/env bash

set -x

raw_path="$1"


get_commit_time() {
    git log -1 --format="%aI" "$1"
}

# this commit just rearranged the project, didn't actually make changes to
# content, which is what really matters here
move_sha=c070cb8daf2dbc1c483368937923a2c0f4318a1b

get_latest_meaningful_commit() {
    local sha=$(git log -1 --pretty=format:%H --follow -- "$1")

    if [ "$sha" == "$move_sha" ]; then
        git log -1 --skip=1 --pretty=format:%H -- "${1#content/}"
    else
        echo "$sha"
    fi
}

latest_meaningful_date=$(get_commit_time "$(get_latest_meaningful_commit "$raw_path")")

# TODO: check if modified: field exists in the first place

sed -ri "s/^modified:.*$/modified: $latest_meaningful_date/" "$raw_path"
