// https://lunrjs.com/guides/index_prebuilding.html
const lunr = require('lunr'),
      stdin = process.stdin,
      stdout = process.stdout,
      buffer = []

stdin.resume()
stdin.setEncoding('utf8')

stdin.on('data', function (data) {
  buffer.push(data)
})

stdin.on('end', function () {
  const documents = JSON.parse(buffer.join(''))

  // TODO: optional fields? like meta-description, tags?
  const idx = lunr(function () {
    this.ref('id')
    this.field('title')
    this.field('body')
    this.field('meta-description')

    documents.forEach(function (doc) {
      this.add(doc)
    }, this)
  })

  stdout.write(JSON.stringify(idx))
})
