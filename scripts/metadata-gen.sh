#!/usr/bin/env bash

raw_path="$1"
contentless_path=${raw_path#content/}
date_in_name=$(echo "$raw_path" | grep -Eo '[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}')


get_commit_time() {
    git log -1 --format="%aI" "$1"
}

get_first_commit() {
    git log --reverse --pretty=format:%H --follow -- "$1" | head -1
}

get_first_commit_time() {
    local first_commit_sha
    first_commit_sha=$(get_first_commit "$1")

    if [ -n "$first_commit_sha" ]; then
        get_commit_time "$first_commit_sha"
    fi
}

# this commit just rearranged the project, didn't actually make changes to
# content, which is what really matters here
move_sha=c070cb8daf2dbc1c483368937923a2c0f4318a1b

get_latest_meaningful_commit() {
    local sha
    sha=$(git log -1 --pretty=format:%H --follow -- "$1")

    if [ "$sha" == "$move_sha" ]; then
        git log -1 --skip=1 --pretty=format:%H -- "${1#content/}"
    else
        echo "$sha"
    fi
}

possible_dates=($(get_first_commit_time "$raw_path") $(get_first_commit_time "$contentless_path") "$date_in_name"T00:00:00Z)

earliest_date=$(printf '%s\0' "${possible_dates[@]}" | sort -z | head -z -n 1 | tr -d "\0")
latest_meaningful_date=$(get_commit_time "$(get_latest_meaningful_commit "$raw_path")")

cat <<EOF > "$raw_path.metadata"
published: $earliest_date
modified: $latest_meaningful_date
EOF
