#!/usr/bin/env bash

yq --front-matter=process --inplace ". *= load(\"$1.metadata\")" "$1"
