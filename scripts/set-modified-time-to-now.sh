#!/usr/bin/env bash

set -x

raw_path="$1"

NOW=${NOW:-$(date -Iseconds)}

# TODO: check if modified: field exists in the first place

sed -ri "s/^modified:.*$/modified: $NOW/" "$raw_path"
