export DEST_DIR ?= $(abspath _site)
export CACHE_DIR ?= $(abspath _cache)
export TMP_DIR ?= $(abspath _cache/tmp)
export CONTENT_DIR ?= $(abspath content)

SITE_CMD ?= cabal new-run doshitan-site --

build:
	cabal new-build doshitan-site

clean:
	cabal new-clean

content-build:
	$(SITE_CMD) build

content-build-prod: | drafts-unlink content-rebuild

content-build-nix: | drafts-unlink
	nix build -L .#content

content-clean:
	$(SITE_CMD) clean

content-check-internal:
	$(SITE_CMD) check --internal-links

content-check-all:
	TMPFILE=$$(mktemp); \
	trap 'rm -rf "$$TMPFILE"' EXIT; \
	echo -e "[checking]\nlocalwebroot=$(DEST_DIR)/" > "$$TMPFILE"; \
	linkchecker \
		--config $$TMPFILE \
		--ignore-url=mailto:hi@replace \
		--timeout 120 \
		--check-extern \
		$(DEST_DIR)/

content-rebuild:
	$(SITE_CMD) rebuild

content-watch:
	$(SITE_CMD) watch

copy-to-ci:
	nix flake archive --to $(CI_SERVER)
	nix copy --to $(CI_SERVER) $(shell nix eval '.#checks.x86_64-linux' --apply 'xs: builtins.concatStringsSep " " (builtins.map (x: ".#$${x}") (builtins.attrNames xs))' --raw)

deploy: deploy-build
	./result/deploy

deploy-build:
	nix build -L .#contentAndDeployScript

deploy-prod: version-assets sync

distclean: content-clean clean font-clean metadata-clean
	rm -f result
	rm -f node_modules

DRAFTS = $(shell find $(CONTENT_DIR)/drafts -type f)

drafts-link:
	for i in $(DRAFTS); do ln -sf "$$i" $(CONTENT_DIR)/posts/"$$(basename $$i)"; done

drafts-unlink:
	for i in $(DRAFTS); do [ -L $(CONTENT_DIR)/posts/"$$(basename $$i)" ] && rm $(CONTENT_DIR)/posts/"$$(basename $$i)"; done

elm2nix-gen: $(CONTENT_DIR)/assets/elm/elm.json
	cd $(CONTENT_DIR)/assets/elm && elm2nix convert > elm-srcs.dat
	cd $(CONTENT_DIR)/assets/elm && elm2nix snapshot > registry.dat

infra-deploy-lambda:
	cd infra && tofu apply -target=aws_lambda_function.cloudfront -target=aws_cloudfront_distribution.site_distribution

metadata_dirs = $(CONTENT_DIR)/{posts,pages,projects}

define mk_metadata_script_run
  git ls-files $(1) -z $(metadata_dirs) | parallel $(2) -0 --
endef

metadata_script_run_all = $(call mk_metadata_script_run)
metadata_script_run_modified = $(call mk_metadata_script_run,--modified --deduplicate)

metadata-gen: SHELL:=bash
metadata-gen:
	$(metadata_script_run_all) ./scripts/metadata-gen.sh {}

metadata-clean: SHELL:=bash
metadata-clean:
	rm $(metadata_dirs)/*.metadata

metadata-merge: SHELL:=bash
metadata-merge:
	$(metadata_script_run_all) ./scripts/metadata-merge.sh {}

metadata-update-modified-from-git: SHELL:=bash
metadata-update-modified-from-git:
	$(metadata_script_run_all) ./scripts/set-modified-time-from-git.sh {}

metadata-update-modified-from-git-staged: SHELL:=bash
metadata-update-modified-from-git-staged:
	$(metadata_script_run_modified) ./scripts/set-modified-time-from-git.sh {}

metadata-update-modified-staged: SHELL:=bash
metadata-update-modified-staged:
	export NOW=$$(date -Iseconds); $(metadata_script_run_modified) ./scripts/set-modified-time-to-now.sh {}

node-gen:
	cd nix/node && nix run 'nixpkgs#nodePackages.node2nix' -- --nodejs-18 -i node-packages.json

AWS_S3_BUCKET ?= some-bucket-name

sync: sync-content sync-headers sync-fix-mime

S3CMD_SYNC_FLAGS := --verbose \
	--human-readable-sizes \
	--no-preserve \
	--no-mime-magic --guess-mime-type \
	--encoding=UTF-8 --add-encoding-exts=css,html,js,txt \

ifndef S3CMD_SYNC_DONT_CF_INVALIDATE
S3CMD_SYNC_FLAGS += --cf-invalidate --cf-invalidate-default-index
endif

ifdef S3CMD_SYNC_REMOVE_OUTDATED
S3CMD_SYNC_FLAGS += --delete-removed
endif

sync-content:
	s3cmd sync $(DEST_DIR)/ s3://$(AWS_S3_BUCKET)/ $(S3CMD_SYNC_FLAGS)

YEAR_IN_SECONDS := 31536000
DAY_IN_SECONDS := 86400

sync-headers:
	s3cmd modify s3://$(AWS_S3_BUCKET) \
	--verbose \
	--recursive \
	--exclude="*" \
	--include="assets/css/*" \
	--include="assets/font/*" \
	--include="assets/js/*" \
	--include="9963DB09FD2A2622CC46FF0320160260F4C18825.txt" \
	--add-header=Cache-Control:max-age=$(YEAR_IN_SECONDS)

	s3cmd modify s3://$(AWS_S3_BUCKET) \
	--verbose \
	--recursive \
	--exclude="*" \
	--include="assets/img/favicon-*" \
	--include="assets/img/icon-*" \
	--add-header=Cache-Control:max-age=$(DAY_IN_SECONDS)

	s3cmd modify s3://$(AWS_S3_BUCKET) \
	--verbose \
	--recursive \
	--exclude="*" \
	--include="*.html" \
	--include="atom.xml" \
	--include="key.txt" \
	--include="search/index.json" \
	--include="search/data.json" \
	--include="*.webmanifest" \
	--include="service-worker.js" \
	--add-header=Cache-Control:s-maxage=$(YEAR_IN_SECONDS),max-age=0,must-revalidate

sync-fix-mime:
	s3cmd modify s3://$(AWS_S3_BUCKET) \
	--verbose \
	--recursive \
	--exclude="*" \
	--include="assets/css/*" \
	--mime-type="text/css"

	s3cmd modify s3://$(AWS_S3_BUCKET) \
	--verbose \
	--recursive \
	--exclude="*" \
	--include="assets/font/*.woff2" \
	--mime-type="font/woff2"

	s3cmd modify s3://$(AWS_S3_BUCKET) \
	--verbose \
	--recursive \
	--exclude="*" \
	--include="assets/js/*" \
	--include="service-worker.js" \
	--mime-type="application/javascript"

	s3cmd modify s3://$(AWS_S3_BUCKET) \
	--verbose \
	--recursive \
	--exclude="*" \
	--include="*.json" \
	--mime-type="application/json"

	s3cmd modify s3://$(AWS_S3_BUCKET) \
	--verbose \
	--recursive \
	--exclude="*" \
	--include=".well-known/matrix/*" \
	--mime-type="application/json"

	s3cmd modify s3://$(AWS_S3_BUCKET) \
	--verbose \
	--recursive \
	--exclude="*" \
	--include="*.webmanifest" \
	--mime-type="application/manifest+json"

version-assets:
	./scripts/version-assets.sh

#
# Font stuff
#

# updating fontello workflow:
# - run `make fontello-open`
# - select which icons are desired
# - click 'Save Session' in the UI
# - run `make fontello-save`
# - (optional) run `make fontello-clean`

FONT_DIR ?= $(CONTENT_DIR)/assets/font
CSS_DIR ?= $(CONTENT_DIR)/assets/css

FONTELLO_CONFIG ?= $(abspath ./fontello-config.json)
FONTELLO_WORKING_DIR ?= $(abspath ./.fontello)
FONTELLO_HOST ?= http://fontello.com

fontello-open:
	mkdir -p $(FONTELLO_WORKING_DIR) || :
	curl --silent --show-error --fail --output $(FONTELLO_WORKING_DIR)/session-id \
		--form "config=@$(FONTELLO_CONFIG)" \
		$(FONTELLO_HOST)
	xdg-open $(FONTELLO_HOST)/`cat $(FONTELLO_WORKING_DIR)/session-id`

fontello-save:
	@if test ! -e "$(FONTELLO_WORKING_DIR)/session-id" ; then \
		echo 'Run `make font-open` first.' >&2 ; \
		exit 128 ; \
		fi
	rm -rf $(FONTELLO_WORKING_DIR)/fontello.{src,zip}
	curl --silent --show-error --fail --output $(FONTELLO_WORKING_DIR)/fontello.zip \
		$(FONTELLO_HOST)/`cat $(FONTELLO_WORKING_DIR)/session-id`/get
  # extract the zip and strip one level from the contents, so instead of
  # `fontello.src/fontello-abc123/{font,css}` we can just work with `fontello.src/{font,css}`
	cd $(FONTELLO_WORKING_DIR) && unzip fontello.zip -d fontello.src && cd fontello.src/fontello-* && mv -f * ../ && cd .. && rmdir fontello-*
  # don't want the svgs
	rm $(FONTELLO_WORKING_DIR)/fontello.src/font/*.svg
  # copy the new files into their proper places
	mv -f $(FONTELLO_WORKING_DIR)/fontello.src/font/* $(FONT_DIR)/
	mv -f $(FONTELLO_WORKING_DIR)/fontello.src/css/fontello-codes.css $(CSS_DIR)/
	mv -f $(FONTELLO_WORKING_DIR)/fontello.src/config.json $(FONTELLO_CONFIG)

fontello-clean:
	rm -rf $(FONTELLO_WORKING_DIR)

MERRIWEATHER_SRC_DIR ?= $(abspath ./.merriweather/fonts)
MERRIWEATHER_DEST_DIR ?= $(abspath ./.merriweather/subset_fonts)

merriweather-nix:
	nix build -L .#merriweather

merriweather-build:
	./scripts/merriweather.sh

merriweather-save:
	cp -f $(MERRIWEATHER_DEST_DIR)/font/* $(FONT_DIR)/
	cp -f $(MERRIWEATHER_DEST_DIR)/merriweather.css $(CSS_DIR)/

# sudo because if nix built it, we just copy the files out of the nix store
# directly, so they are marked read-only
merriweather-clean:
	sudo rm -rf .merriweather

ANDADA_SRC_DIR ?= $(abspath ./.andada/fonts)
ANDADA_DEST_DIR ?= $(abspath ./.andada/subset_fonts)

andada-pro-nix:
	nix build -L .#andada-pro

andada-pro-build:
	./scripts/andada-pro.sh

andada-pro-save:
	cp -f $(ANDADA_DEST_DIR)/font/* $(FONT_DIR)/
	cp -f $(ANDADA_DEST_DIR)/andada-pro.css $(CSS_DIR)/

# sudo because if nix built it, we just copy the files out of the nix store
# directly, so they are marked read-only
andada-pro-clean:
	sudo rm -rf .andada-pro

FIRA_MONO_SRC_DIR ?= $(abspath ./.fira-mono/fonts/otf)
FIRA_MONO_DEST_DIR ?= $(abspath ./.fira-mono/subset_fonts)

fira-mono-nix:
	nix build -L .#firaMono

fira-mono-build:
	./scripts/fira-mono.sh

fira-mono-save:
	cp -f $(FIRA_MONO_DEST_DIR)/font/* $(FONT_DIR)/
	cp -f $(FIRA_MONO_DEST_DIR)/fira-mono.css $(CSS_DIR)/

# sudo because if nix built it, we just copy the files out of the nix store
# directly, so they are marked read-only
fira-mono-clean:
	sudo rm -rf .fira-mono

font-clean: fontello-clean merriweather-clean fira-mono-clean

update-nixpkgs: ## Update nixpkgs used for project
	nix flake lock --update-input nixpkgs
