{ compiler ? "default", withProfiling ? false }:

newPkgs: oldPkgs: {
  merriweather = oldPkgs.stdenv.mkDerivation rec {
    pname = "merriweather";
    version = "2.100";

    src = oldPkgs.fetchFromGitHub {
      owner = "SorkinType";
      repo = "Merriweather";
      rev = "2acee34c68dd61054c82078f14dc6cc38eb82142";
      sha256 = "sha256-nTbVVGuPT8FFDM+tS2jEkGBTaPdahT6YF6f6NWRcP6M=";
    };

    phases = [ "unpackPhase" "installPhase" ];

    installPhase = ''
      mkdir $out
      mv fonts/* $out/
    '';
  };

  andada-pro = oldPkgs.stdenv.mkDerivation rec {
    pname = "andada-pro";
    version = "3b6e9f2fb";

    src = oldPkgs.fetchFromGitHub {
      owner = "huertatipografica";
      repo = "Andada-Pro";
      rev = "3b6e9f2fb3263b0d928ff0c8dcddaabdf3856235";
      sha256 = "sha256-fRjXJqgA+L0rhbbYcMNMfAZZ57/8VrhwxDBhdkoLBbM=";
    };

    phases = [ "unpackPhase" "installPhase" ];

    installPhase = ''
      mkdir $out
      mv fonts/* $out/
    '';
  };

  lib = oldPkgs.lib // (with oldPkgs.lib; rec {
    applyFuncs = funcs: drv: foldl' (drv': f: f drv') drv funcs;
  });

  origHaskellPackages = if compiler == "default" then
    oldPkgs.haskellPackages
  else
    oldPkgs.haskell.packages."${compiler}";

  modifiedHaskellPackages = newPkgs.origHaskellPackages.override {
    overrides = newHaskellPkgs: oldHaskellPkgs: {
      mkDerivation = args:
        oldHaskellPkgs.mkDerivation
        (args // { enableLibraryProfiling = withProfiling; });

      # the tests seem to take forever to run
      crypton-x509-validation = with oldPkgs.haskell.lib;
        overrideCabal oldHaskellPkgs.crypton-x509-validation
        (drv: rec { doCheck = false; });

      # one test randomly fails
      tls = with oldPkgs.haskell.lib;
        overrideCabal oldHaskellPkgs.tls (drv: rec { doCheck = false; });
    };
  };

  haskellPackages = newPkgs.modifiedHaskellPackages;

  nodePackages = import ./node {
    pkgs = oldPkgs;
    nodejs = newPkgs.nodejs;
  };

  contentNodeRuntimeEnv = oldPkgs.buildEnv {
    name = "content-runtime";
    paths = builtins.attrValues newPkgs.nodePackages;
  };
}
