---
title: ICT Game Jam 2015
meta-description: Diving head first into game development with Elm
tags: Golden Ear, Elm
published: 2015-09-03T19:57:15-05:00
modified: 2015-09-03T19:57:15-05:00
---

It's been about a month since the first ever ICT Game Jam ended. I took part
mostly just to show my support for local things like this as I'm not all that
interested in game development otherwise. That was freeing in a way, I went in
just to have fun (which I think everyone else did too), no pressure on myself
to even have anything functional at the end! Of course, I'd love to have
something neat on the other side of the weekend, but that wasn't my goal.

So, seeing as I wasn't really interested in game development -- and all the major
"free" game toolkits (Unity, Unreal, etc.) don't play nice with GNU/Linux or are
in a language I don't enjoy, e.g., Phaser -- I decided if I was gonna do it I
wanted it to at least be interesting (to me if no one else). That meant choosing
some tools I was actually interested in and at least knew enough to get started
in them, which boiled down to either Haskell or Elm. I ended up deciding on Elm,
mainly because I've been wanting to do something with it and this seemed like a
good opportunity.

I came up with a few abstract concepts I felt I could have a chance at actually
making and either adapt to whatever theme was chosen or just ignore the theme
and do my own thing (^_^).

The night before the event started I whipped up a little prototype (if it can
even be called that) of one of my ideas just to make sure I had all of the Elm
platform working together and got over the initial language speed bumps ("How do
I compose functions again? Oh, `<<` or `>>`, cool." and so on).

Given the technical restrictions I had decided upon and my attitude towards the
whole affair, I was fully prepared to do something on my own; very few people
would want to learn a language they'd (likely) never heard of, in a style of
programming they'd (likely) never used, with someone who didn't want to stay up
all night and didn't care about winning.

The first night of the event (a Friday) was mostly spent just hanging out with
the folks I knew (including giving a quick into to Haskell to one of them).
Towards the end of the night, there was actually a group of folks on board with
building out one of my ideas (the one I had "prototyped") in Elm. More
precisely, they were on board with *me* building the game in Elm and them doing
everything else (^_^). I made it perfectly clear that I couldn't even promise
we'd have something deliverable by Sunday! I was pretty confident that we'd have
*something*, but I'd never done a game before and never seriously worked in Elm,
so I wasn't about to have people depend on me to knock it out of the park in 24
hours. The crazy fools went along with me anyway. So we fleshed out the idea a
bit and made it in theme for the event. Then we all went home around 10pm.

Started up the next day around 9-10am and quickly got to work. I spent a good
amount of time, I think it was like 3-4 hours, getting the gameplay up to the
point of my "prototype", but of course in a more modularized/nicer fashion with
a start screen that had buttons and such. The thing that took the longest was
just reading the Elm docs about the "Elm Architecture" and trying to map the
Mailbox/Address and Signal stuff into working code, threading the stuff through
to the now separate Game module. By the end of Saturday, the basic gameplay was
pretty much done, a lot of the themeing was in place -- thanks to the other
members of the team -- and we were pretty happy. We all went home by 10-11pm.

Started Sunday again around 9am. We dropped in all the audio, tweaked some of
the art, and just tried to polish the rest off. We were to deliver our game for
the judge's to play by noon. It wasn't long after that the public started coming
in and playing all the games. Of course it's hard to just stop working on the
thing, especially when Elm makes it hard to get something running that's broken,
so I kept pushing some updates after the "deadline" (with official approval)
incorporating some of the early feedback we were getting. They were just minor
things, a small progress bar across the top that gave a sense of how much of the
conversation was left to listen to, a tip on the home screen on how to play,
getting the game audio to restart on every new game instead of having to reload
the app and a slight tweak to some colors. Around 2pm it was our turn pitch our
game to the judges and around 4pm they got up and announced the winners for the
Community and Judge's Choice awards[^1], among other things. Then we all went
home.

We haven't done much with it since, though we do plan on making it better at some
point. I'd like to add some localStorage support to store your scores and other
state. In the mockups we had a richer "debriefing" section where you have to
answer questions about key points in conversation to actually "win", that would
be neat to implement. More "levels"/situations/conversations would also be good.
Having the ring wiggle to the actual sound wave of the conversation would be
pretty sweet too, though that's pretty much a pipe dream, dunno if I really want
to dive into implementing that. If the Touch library was well supported in
mobile browsers, we'd actually automatically have a mobile game as I made
everything scalable from the start, so that would be nice to get figured out.

All in all it was pretty fun. All the code was done by me from scratch starting
Saturday morning and the other guys did a great job making/collecting all the
other assets and really making the game what it turned out to be. It really
would have just been super simple geometric shapes and solid colors if I had
done it myself. The atmosphere is what makes the game at all interesting to play
and none of that was me.

All the other teams had some fun ideas too and I think everyone really enjoyed
the event. If you're interested, you can take a look at the code and give the
game a try from the [project page](/golden-ear/).

[^1]: We ended up winning the Judge's Choice, so that was neat.
