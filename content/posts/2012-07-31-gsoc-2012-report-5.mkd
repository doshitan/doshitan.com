---
title: "GSoC 2012: Report 5"
meta-description: Approaching the end of GSoC and getting things ready for review
tags: GNOME, GSoC
published: 2012-07-31T00:00:00Z
modified: 2016-08-20T14:34:42-05:00
---

This report covers the fifth two week block of coding time, July 16 -- July
29. Getting close to the end folks, just a few more weeks.

# Week 9 #
Implemented show/hide methods for the Dash and made use of them while
searching. Started work on treating the workspace thumbnails more like the
Dash, i.e., an element added to the overview on its own, not tied to something
else.

# Week 10 #
Finished splitting the workspace thumbnails from the window previews. Added
hide/show methods to ThumbnailsBox that slide the thumbnails out of/in to view
and made use of them while searching.

# Looking Forward #
I need to package up all the separate work I have been doing and get it
attached to some bugs for proper review.

[gnomeSearchDesign]: https://web.archive.org/web/20120819185611/https://live.gnome.org/GnomeShell/Design/Whiteboards/Search
