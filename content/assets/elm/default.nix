{ pkgs
}:

with pkgs;

let
  mkDerivation =
    { srcs ? ./elm-srcs.nix
    , src
    , name
    , srcdir ? "./src"
    , targets ? []
    , registryDat ? ./registry.dat
    }:
    stdenv.mkDerivation {
      inherit name;

      src = builtins.filterSource (name: type: let baseName = baseNameOf (toString name); in ! (
        (type == "directory" && (baseName == "elm-stuff"))
      )) src;

      buildInputs = [ elmPackages.elm ];

      buildPhase = pkgs.elmPackages.fetchElmDeps {
        elmPackages = import srcs;
        elmVersion = "0.19.1";
        inherit registryDat;
      };

      installPhase = let
        elmfile = module: "${srcdir}/${builtins.replaceStrings ["."] ["/"] module}.elm";
      in ''
        mkdir -p $out/share/doc
        ${lib.concatStrings (map (module: ''
          echo "compiling ${elmfile module}"
          elm make ${elmfile module} --optimize --output $out/${module}.js --docs $out/share/doc/${module}.json
        '') targets)}
      '';
    };
in mkDerivation {
  name = "search-app-0.1.0";
  srcs = ./elm-srcs.nix;
  src = ./.;
  targets = ["SearchMain"];
  srcdir = "./.";
}

