port module SearchMain exposing (Config, Model, Msg(..), State(..), Status(..), Visibility(..), init, loadIndex, main, performSearch, show, subscriptions, update, view)

import Browser
import Browser.Dom as Dom
import Browser.Events exposing (onKeyUp)
import Dict exposing (Dict)
import Html exposing (Attribute, Html, div, input, text)
import Html.Attributes exposing (..)
import Html.Events exposing (on, onClick, onInput)
import Http
import Json.Decode as D exposing (Decoder)
import Json.Encode as E
import Task
import Url.Builder as U


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type alias Model =
    { config : Config
    , state : State
    , visibility : Visibility
    , searchIdx : Status E.Value
    , searchData : Status SearchData
    , searchInput : String
    , searchResults : SearchResults
    }


type alias Config =
    { searchIdxUrl : String
    , searchDataUrl : String
    , modalId : String
    }


type State
    = Initialized


type Visibility
    = Showing
    | Hidden


type Status a
    = NotFetched
    | Loading
    | LoadingSlowly
    | Loaded a
    | Failed


type alias SearchResults =
    List SearchResult


type alias SearchResult =
    { ref : String
    , score : Float
    , matchData : E.Value
    }


searchResultsDecoder : Decoder SearchResults
searchResultsDecoder =
    D.list searchResultDecoder


searchResultDecoder : Decoder SearchResult
searchResultDecoder =
    D.map3 SearchResult
        (D.field "ref" D.string)
        (D.field "score" D.float)
        (D.field "matchData" D.value)


type alias SearchData =
    Dict String SearchDatum


type alias SearchDatum =
    { title : String }


searchDataDecoder : Decoder SearchData
searchDataDecoder =
    D.dict searchDatumDecoder


searchDatumDecoder : Decoder SearchDatum
searchDatumDecoder =
    D.map SearchDatum
        (D.field "title" D.string)


init : Config -> ( Model, Cmd Msg )
init userConfig =
    ( { config = userConfig
      , state = Initialized
      , visibility = Hidden
      , searchIdx = NotFetched
      , searchData = NotFetched
      , searchInput = ""
      , searchResults = []
      }
    , Cmd.none
    )



-- UPDATE


type Msg
    = Show
    | Hide
    | GotSearchIdx (Result Http.Error String)
    | GotSearchData (Result Http.Error SearchData)
    | SearchFieldInput String
    | SearchResultsUpdate (Result D.Error SearchResults)
    | NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Show ->
            let
                ( searchIdxStatus, searchIdxCmd ) =
                    fetchIfNeeded
                        model.searchIdx
                        (Http.get
                            { url = model.config.searchIdxUrl
                            , expect = Http.expectString GotSearchIdx
                            }
                        )

                ( searchDataStatus, searchDataCmd ) =
                    fetchIfNeeded
                        model.searchData
                        (Http.get
                            { url = model.config.searchDataUrl
                            , expect = Http.expectJson GotSearchData searchDataDecoder
                            }
                        )
            in
            ( { model
                | visibility = Showing
                , searchIdx = searchIdxStatus
                , searchData = searchDataStatus
              }
            , Cmd.batch
                [ modalOpen ()
                , searchIdxCmd
                , searchDataCmd
                , focusSearchField
                ]
            )

        Hide ->
            ( { model | visibility = Hidden }, modalClose () )

        GotSearchIdx result ->
            case result of
                Ok body ->
                    let
                        jsonBodyString =
                            E.string body
                    in
                    ( { model | searchIdx = Loaded jsonBodyString }, loadIndex jsonBodyString )

                Err _ ->
                    ( { model | searchIdx = Failed }, Cmd.none )

        GotSearchData result ->
            case result of
                Ok body ->
                    ( { model | searchData = Loaded body }, Cmd.none )

                Err _ ->
                    ( { model | searchData = Failed }, Cmd.none )

        SearchFieldInput content ->
            ( { model | searchInput = content }, performSearch (E.string content) )

        SearchResultsUpdate results ->
            case results of
                Ok res ->
                    ( { model | searchResults = res }, Cmd.none )

                Err _ ->
                    -- TODO: parse failed, unlikely, should we care?
                    ( model, Cmd.none )

        NoOp ->
            ( model, Cmd.none )


searchFieldId =
    "search-field"


focusSearchField : Cmd Msg
focusSearchField =
    Task.attempt (\_ -> NoOp) (Dom.focus searchFieldId)


fetchIfNeeded : Status a -> Cmd msg -> ( Status a, Cmd msg )
fetchIfNeeded status fetchCmd =
    case status of
        NotFetched ->
            ( Loading, fetchCmd )

        x ->
            ( x, Cmd.none )



-- SUBSCRIPTIONS
-- incoming


port show : (E.Value -> msg) -> Sub msg


port hide : (E.Value -> msg) -> Sub msg


port toggleVisibility : (E.Value -> msg) -> Sub msg


port searchResults : (E.Value -> msg) -> Sub msg



-- outgoing


port loadIndex : E.Value -> Cmd msg


port performSearch : E.Value -> Cmd msg


port modalOpen : () -> Cmd msg


port modalClose : () -> Cmd msg


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ show (always Show)
        , hide (always Hide)
        , toggleVisibility
            (always
                (if model.visibility == Showing then
                    Hide

                 else
                    Show
                )
            )
        , searchResults (D.decodeValue searchResultsDecoder >> SearchResultsUpdate)
        , onKeyUp keyPressToMsg
        ]


keyPressToMsg : Decoder Msg
keyPressToMsg =
    D.map keyToMsg (D.field "key" D.string)


keyToMsg : String -> Msg
keyToMsg string =
    case string of
        "Escape" ->
            Hide

        _ ->
            NoOp



-- VIEW


view : Model -> Html Msg
view model =
    let
        modalAttrs =
            case model.visibility of
                Showing ->
                    [ attribute "aria-modal" "true" ]

                Hidden ->
                    [ attribute "aria-hidden" "true" ]
    in
    div
        (modalAttrs
            ++ [ id model.config.modalId
               , classList [ ( "modal", True ), ( "show", model.visibility == Showing ) ]
               , attribute "role" "dialog"
               , attribute "tabindex" "-1"
               ]
        )
        [ div
            [ class "modal-dialog", attribute "role" "document" ]
            [ div
                [ class "modal-content" ]
                [ div
                    [ class "modal-body" ]
                    [ viewModalContent model ]
                ]
            ]
        , div [ class "modal-backdrop", onClick Hide ] []
        ]


viewModalContent : Model -> Html Msg
viewModalContent model =
    div []
        [ input
            [ id searchFieldId
            , placeholder "Search"
            , type_ "search"
            , value model.searchInput
            , onInput SearchFieldInput
            ]
            []

        -- TODO: maybe as a dropdown at end of input
        , Html.ul [ class "meta navbar list-inline" ]
            [ Html.li [] [ Html.a [ href <| ddgSearchUrl model.searchInput ] [ text "on ddg" ] ]
            , Html.li [] [ Html.button [ class "text-button", type_ "button", onClick Hide ] [ text "close" ] ]
            ]
        , viewSearchResults model
        ]


viewSearchResults : Model -> Html Msg
viewSearchResults model =
    case model.state of
        Initialized ->
            case ( model.searchData, model.searchIdx ) of
                ( Loaded searchData, Loaded _ ) ->
                    Html.ul [ class "post-list" ] <| List.map (viewSearchResult searchData) model.searchResults

                ( Failed, _ ) ->
                    text "Error fetching data..."

                ( _, Failed ) ->
                    text "Error fetching data..."

                ( _, _ ) ->
                    text "Loading..."


viewSearchResult : SearchData -> SearchResult -> Html Msg
viewSearchResult searchData result =
    Html.li []
        [ Html.a [ href result.ref ] [ text <| Maybe.withDefault result.ref <| Maybe.map .title <| Dict.get result.ref searchData ]
        ]


ddgSearchUrl : String -> String
ddgSearchUrl terms =
    U.custom
        (U.CrossOrigin "https://duckduckgo.com")
        []
        [ U.string "q" <| "site:doshitan.com " ++ terms ]
        Nothing
