-- | Print the compiled stylesheet

module Main where

import Prelude (IO)
import Clay
import Css

main :: IO ()
main = putCss theStylesheet

