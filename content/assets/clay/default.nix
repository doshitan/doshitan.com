{ pkgs }:

rec {
  css-exec = pkgs.haskellPackages.callCabal2nix "doshitan-css" ./. { };

  css = pkgs.stdenv.mkDerivation rec {
    name = "doshitan-css-${version}";
    version = "0.0.0";
    phases = [ "buildPhase" ];
    buildInputs = [ css-exec ];

    LOCALE_ARCHIVE = pkgs.lib.optionalString pkgs.stdenv.isLinux
      "${pkgs.glibcLocales}/lib/locale/locale-archive";
    LANG = "en_US.UTF-8";
    LC_TYPE = "en_US.UTF-8";

    buildPhase = ''
      css > $out
    '';
  };
}
