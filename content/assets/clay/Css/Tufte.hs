module Css.Tufte where

import           Clay
import qualified Clay.Media
import           Data.Monoid     ((<>))
import           Data.Text       hiding (span)
import           Prelude         hiding (div, rem, span)

data Config = Config
  { cfgTextColor :: Color
  , cfgBackgroundColor :: Color
  , cfgFontFamily :: [Text]
  , cfgFontFamilySans :: [Text]
  , cfgFontFamilyMono :: [Text]
  , cfgBaseFontSize :: Size LengthUnit
  , cfgContentFontSize :: Size LengthUnit
  , cfgCodeFontSize :: Size LengthUnit
  , cfgHighlightColor :: Color
  , cfgLinkCss :: Color -> Css
  , cfgSmallScreenBreakpoint :: Size LengthUnit
  , cfgCodeBackgroundColor :: Color
  }

tufteConfig :: Config
tufteConfig = Config
  { cfgTextColor = "#111"
  , cfgBackgroundColor = "#fffff8"
  , cfgFontFamily =
    [ "et-book"
    , "Palatino"
    , "Palatino Linotype"
    , "Palatino LT STD"
    , "Book Antiqua"
    , "Georgia"
    ]
  , cfgFontFamilySans =
    [ "Gill Sans"
    , "Gill Sans MT"
    , "Calibri"
    ]
  , cfgFontFamilyMono =
    [ "Consolas"
    , "Liberation Mono"
    , "Menlo"
    , "Courier"
    ]
  , cfgBaseFontSize = px 15
  , cfgContentFontSize = rem 1.4
  , cfgCodeFontSize = rem 1
  , cfgHighlightColor = "#b4d5fe"
  , cfgLinkCss = tufteLinkCss
  , cfgSmallScreenBreakpoint = px 760
  , cfgCodeBackgroundColor = transparent
  }

doshitanConfig :: Config
doshitanConfig = tufteConfig
  { cfgTextColor = grayDark
  , cfgBackgroundColor = white
  -- "Adobe Garamond Pro", "Georgia", "Times New Roman",Times,serif
  -- -apple-system, BlinkMacSystemFont, "Segoe UI", "Droid Sans", "Helvetica Neue", "PingFang SC", "Hiragino Sans GB", "Droid Sans Fallback", "Microsoft YaHei", sans-serif
  , cfgFontFamily = ["Andada Pro", "Merriweather", "Georgia", "Times New Roman", "Times"]
  , cfgFontFamilySans = ["Helvetica Neue", "Helvetica", "Arial"]
  , cfgFontFamilyMono = ["Fira Mono", "Monaco", "Menlo", "Consolas", "Courier New"]
  , cfgBaseFontSize = px 14
  , cfgCodeFontSize = px 16
  , cfgHighlightColor = "#396622"
  , cfgLinkCss = simplifiedLinkCss
  , cfgSmallScreenBreakpoint = px 820
  , cfgCodeBackgroundColor = grayLighter
  }

doshitanDarkConfig :: Config
doshitanDarkConfig = doshitanConfig
  { cfgTextColor = "#e8e6e3"
  , cfgBackgroundColor = "#181a1b"
  , cfgCodeBackgroundColor = "#282828"
  }

tufte :: Config -> Css
tufte cfg = do
  html ?
    fontSize (cfgBaseFontSize cfg)

  body ? do
    width (pct 87.5)
    marginLeft auto
    marginRight auto
    paddingLeft (pct 12.5)
    fontFamily (cfgFontFamily cfg) [serif]
    backgroundColor (cfgBackgroundColor cfg)
    color (cfgTextColor cfg)
    maxWidth (px 1400)
    "counter-reset" -: "sidenote-counter"

  h1 ? do
    fontWeight (weight 400)
    marginTop (rem 4)
    marginBottom (rem 1.5)
    fontSize (rem 3.2)
    lineHeight (unitless 1)

  h2 ? do
    fontStyle italic
    fontWeight (weight 400)
    marginTop (rem 2.1)
    marginBottom (cfgContentFontSize cfg)
    fontSize (rem 2.2)
    lineHeight (unitless 1)

  h3 ? do
    fontStyle italic
    fontWeight (weight 400)
    marginTop (rem 2)
    marginBottom (cfgContentFontSize cfg)
    fontSize (rem 1.7)
    lineHeight (unitless 1)

  hr ? do
    display block
    height (px 1)
    width (pct 55)
    "border" -: "0"
    borderTop (px 1) solid "#ccc"
    sym2 margin (em 1) nil
    "padding" -: "0"

  p # ".subtitle" ? do
    fontStyle italic
    marginTop (rem 1)
    marginBottom (rem 1)
    fontSize (rem 1.8)
    display block
    lineHeight (unitless 1)

  -- TODO: old style numerals?
  -- ".numeral" ? fontFamily

  ".danger" ? color red

  article ? do
    sym2 padding (rem 5) nil

  section ? do
    paddingTop (rem 1)
    paddingBottom (rem 1)

  (p <> dl <> ol <> ul) ? do
    fontSize (cfgContentFontSize cfg)
    lineHeight (rem 2)

  p ? do
    marginTop (cfgContentFontSize cfg)
    marginBottom (cfgContentFontSize cfg)
    paddingRight nil
    verticalAlign vAlignBaseline

  -- Chapter Epigraphs
  div # ".epigraph" ? do
    sym2 margin (rem 5) nil

    blockquote <? do
      marginTop (rem 3)
      marginBottom (rem 3)

      footer <? do
        fontStyle normal
        cite <? fontStyle italic

    (blockquote <> (blockquote |> p)) <? fontStyle italic

  blockquote ? do
    fontSize (cfgContentFontSize cfg)
    p ? do
      width (pct 55)
      marginRight (px 40)
    footer ? do
      width (pct 55)
      fontSize (rem 1.1)
      textAlign (alignSide sideRight)

  section ? do
    (p <> footer <> table) <? width (pct 55)
    (dl <> ol <> ul) <? do
      width (pct 50)
      "-webkit-padding-start" -: "5%"

  ( "dt:not(:first-child)" <> "li:not(:first-child)") ? marginTop (rem 0.25)

  figure ? do
    "padding" -: "0"
    "border" -: "0"
    fontSize (pct 100)
    "font" -: "inherit"
    verticalAlign vAlignBaseline
    maxWidth (pct 55)
    "-webkit-margin-start" -: "0"
    "-webkit-margin-end" -: "0"
    margin nil nil (rem 3) nil
    ".fullwidth" & do
      figcaption ? marginRight (pct 24)

  figcaption ? do
    float floatRight
    clear clearRight
    marginTop nil
    marginBottom nil
    fontSize (rem 1.1)
    lineHeight (unitless 1.6)
    verticalAlign vAlignBaseline
    position relative
    maxWidth (pct 40)

  ((a # link) <> (a # visited)) ? color inherit

  ".no-tufte-underline:link" ? noUnderline

  (a # link <> ".tufte-underline" <> ".hover-tufte-underline:hover") ? cfgLinkCss cfg (cfgBackgroundColor cfg)

  -- In upstream Tufte, this is specifically applied to a:link, but might as
  -- well just apply a simplified version to everything for consistency
  selection & selectionCss
  "::-moz-selection" & selectionCss

  -- TODO: I don't know why this is done, just positioning it with the other
  -- rules for it seems to work fine
  -- query Clay.Media.screen [Clay.Stylesheet.Feature "-webkit-min-device-pixel-ratio" (Just "0")] $
  --   (a # link <> ".tufte-underline" <> ".hover-tufte-underline:hover") ? ("background-position-y" -: "87%, 87%, 87%")

  ("a.no-tufte-underline:link::selection" <> "a.no-tufte-underline:link::-moz-selection") ? noUnderline

  img ? maxWidth (pct 100)

  (".sidenote" <> ".marginnote") ? do
    float floatRight
    clear clearRight
    marginRight (pct (-60))
    width (pct 50)
    marginTop (rem 0.3)
    marginBottom nil
    fontSize (rem 1.1)
    lineHeight (unitless 1.3)
    verticalAlign vAlignBaseline
    position relative

  ".sidenote-number" ? do
      "counter-increment" -: "sidenote-counter"
      after & do
        -- TODO: old style numerals
        -- fontFamily
        position relative
        verticalAlign vAlignBaseline
        "content" -: "counter(sidenote-counter)"
        fontSize (rem 1)
        top (rem (-0.5))
        left (rem 0.1)

  ".sidenote" ? do
      before & do
        -- duplicated first few lines with after due to
        -- https://github.com/sebastiaanvisser/clay/issues/53
        -- TODO: old style numerals
        -- fontFamily
        position relative
        verticalAlign vAlignBaseline
        "content" -: "counter(sidenote-counter) ' '"
        fontSize (rem 1)
        top (rem (-0.5))

  blockquote ? (".sidenote" <> ".marginnote") ? do
    marginRight (pct (-82))
    minWidth (pct 59)
    textAlign (alignSide sideLeft)

  (div # ".fullwidth" <> table # ".fullwidth") ? width (pct 100)

  div # ".table-wrapper" ? do
    overflowX auto
    -- TODO: what
    -- fontFamily

  ".sans" ? do
    -- TODO: sans font family
    fontFamily (cfgFontFamilySans cfg) [sansSerif]
    "letter-spacing" -: "0.3rem"

  (code <> (pre |> code)) ? do
    fontFamily (cfgFontFamilyMono cfg) [monospace]
    fontSize (cfgCodeFontSize cfg)
    lineHeight (unitless 1.42)
    -- Prevent adjustments of font size after orientation changes in iOS. See
    -- https://github.com/edwardtufte/tufte-css/issues/81#issuecomment-261953409
    "-webkit-text-size-adjust" -: "100%"

  ".sans > code" ? fontSize (rem 1.2)

  (h1 <> h2 <> h3) ? code <? fontSize (em 0.80)

  (".marginnote" <> ".sidenote") ? code <? fontSize (rem 1)

  pre |> "code" ? do
    fontSize (cfgCodeFontSize cfg)
    width (pct 52.5)
    marginLeft (pct 2.5)
    overflowX auto
    display block

  "pre.fullwidth > code" ? do
    width (pct 90)

  ".fullwidth" ? do
    maxWidth (pct 90)
    clear both

  span # ".newthought" ? do
    fontVariant smallCaps
    fontSize (em 1.2)

  input # ".margin-toggle" ? display displayNone

  label # ".sidenote-number" ? display inline

  label # ".margin-toggle:not(.sidenote-number)" ? display displayNone

  ".iframe-wrapper" ? do
    position relative
    paddingBottom (pct 56.25) -- 16:9
    paddingTop (px 25)
    height nil

    iframe ? do
      position absolute
      top nil
      left nil
      width (pct 100)
      height (pct 100)

  query Clay.Media.screen [Clay.Media.maxWidth (cfgSmallScreenBreakpoint cfg)] $ do
    body ? do
      width (pct 84)
      paddingLeft (pct 8)
      paddingRight (pct 8)

    section ? do
      (p <> footer <> table) <? width (pct 100)
      (dl <> ol <> ul) <? width (pct 90)

    (pre |> code) ? width (pct 97)

    figure ? maxWidth (pct 90)

    (figcaption <> "figure.fullwidth figcaption") ? do
      marginRight (pct 0)
      maxWidth none

    blockquote ? do
      marginLeft (em 1.5)
      marginRight (em 0)
      (p <> footer) <? width (pct 100)

    "label.margin-toggle:not(.sidenote-number)" ? display inline

    ".sidenote-number" ? do
        after & do
          -- make the click target a little bigger for mobile users, just adding
          -- some padding didn't quite look right, brackets are little busier
          -- visually, but look better I think
          "content" -: "'[' counter(sidenote-counter) ']'"

    (".sidenote" <> ".marginnote") ? display displayNone

    (".margin-toggle:checked + .sidenote" <> ".margin-toggle:checked + .marginnote") ? do
      display block
      float floatLeft
      left (rem 1)
      clear both
      width (pct 95)
      -- TODO: open Clay issue about mixed units
      -- TODO: use once released
      -- https://github.com/sebastiaanvisser/clay/commit/25a90636796f8e02a5dd5be35067fe3296c7d5fa
      -- sym2 margin (rem 1) (pct 2.5)
      "margin" -: "1rem 2.5%"
      verticalAlign vAlignBaseline
      position relative

    label ? cursor pointer

    (div # ".table-wrapper" <> table) ? width (pct 85)

    -- TODO: redundant?
    img ? width (pct 100)
  where
    selectionCss = do
      -- TODO: should the text color change with background color, e.g., dark mode?
      color "#fff"
      "text-shadow" -: "none"
      backgroundColor (cfgHighlightColor cfg)

noUnderline :: Css
noUnderline = important $ do
  "background" -: "unset"
  "text-shadow" -: "unset"

tufteLinkCss :: Color -> Css
tufteLinkCss _ = do
  textDecoration none
  "background" -: "-webkit-linear-gradient(#fffff8, #fffff8), -webkit-linear-gradient(#fffff8, #fffff8), -webkit-linear-gradient(currentColor, currentColor)"
  "background" -: "linear-gradient(#fffff8, #fffff8), linear-gradient(#fffff8, #fffff8), linear-gradient(currentColor, currentColor)"
  "-webkit-background-size" -: "0.05em 1px, 0.05em 1px, 1px 1px"
  "-moz-background-size" -: "0.05em 1px, 0.05em 1px, 1px 1px"
  "background-size" -: "0.05em 1px, 0.05em 1px, 1px 1px"
  "background-repeat" -: "no-repeat, no-repeat, repeat-x"
  "text-shadow" -: "0.03em 0 #fffff8, -0.03em 0 #fffff8, 0 0.03em #fffff8, 0 -0.03em #fffff8, 0.06em 0 #fffff8, -0.06em 0 #fffff8, 0.09em 0 #fffff8, -0.09em 0 #fffff8, 0.12em 0 #fffff8, -0.12em 0 #fffff8, 0.15em 0 #fffff8, -0.15em 0 #fffff8"
  "background-position" -: "0% 93%, 100% 93%, 0% 93%"

simplifiedLinkCss :: Color -> Css
simplifiedLinkCss backgroundColor = do
  textDecoration none
  "background" -: "-webkit-linear-gradient(currentColor, currentColor)"
  "background" -: "linear-gradient(currentColor, currentColor)"
  "-webkit-background-size" -: "100% 1px"
  "-moz-background-size" -: "100% 1px"
  "background-size" -: "100% 1px"
  "background-repeat" -: "no-repeat"
  "text-shadow" -: Data.Text.intercalate "," (fmap (<> bgColorTxt)
    [ "0.03em 0 "
    , "-0.03em 0 "
    , "0 0.03em "
    , "0 -0.03em "
    , "0.06em 0 "
    , "-0.06em 0 "
    , "0.09em 0 "
    , "-0.09em 0 "
    , "0.12em 0 "
    , "-0.12em 0 "
    , "0.15em 0 "
    , "-0.15em 0 "
    , "0.18em 0 "
    , "-0.18em 0 "
    ])
  "background-position" -: "center bottom 5%"
  where
    plainValueRepr = plain . unValue . value
    bgColorTxt = plainValueRepr backgroundColor

-- Better grays
grayDarker, grayDark, gray, grayLight, grayLighter :: Color
grayDarker  = grayish 34  -- #222
grayDark    = grayish 51  -- #333
gray        = grayish 85  -- #555, FIXME: shadows Clay's gray?
grayLight   = grayish 153 -- #999
grayLighter = grayish 238 -- #eee
