module Css.Utils where

import Prelude     (undefined)
import Data.Monoid ((<>), Monoid)

-- Lifted from Data.Semigroup
-- Reduce a non-empty list with @\<\>@
sconcat :: Monoid a => [a] -> a
sconcat [] = undefined
sconcat (a:as) = go a as
  where
    go b (c:cs) = b <> go c cs
    go b []     = b
