module Css where

import           Clay           hiding (gray)
import qualified Clay.Media
import qualified Clay.Stylesheet
import qualified Clay.Text
import           Data.Monoid    ((<>))
import           Prelude        hiding (div, rem, span)

import           Css.Tufte
import           Css.Utils

linkHoverColor = "#359c00"

theStylesheet :: Css
theStylesheet = do
  tufte doshitanConfig

  iconStyles
  modalStyles

  body ? do
    "font-variant-numeric" -: "oldstyle-nums"
    textRendering optimizeLegibility

  span # ".smallcaps" ? do
    fontVariant smallCaps

  -- The site's font builds include true small caps, which look a little off
  -- with the font size increase included in Tufte CSS, reset to the standard
  -- font size
  span # ".newthought" ? do
    fontSize inherit

  -- Headers inside the main content probably shouldn't be very long, but in the
  -- rare cases they are, they should be constrained to the same width as the
  -- paragraphs that follow and not bleed into the margin area
  section ? do
    sconcat [h1, h2, h3, h4, h5] <? width (pct 55)

  smallScreen doshitanConfig $ do
    section ? do
      sconcat [h1, h2, h3, h4, h5] <? width (pct 100)

  -- tufte-css only includes -webkit version
  section ? do
    (dl <> ol <> ul) <? do
      "-moz-padding-start" -: "5%"
      "padding-inline-start" -: "5%"

  -- I don't use H4s very often and generally things shouldn't be nested that
  -- deep, but occasionally I want them and they shouldn't be smaller than the
  -- normal text
  h4 ? do
    fontStyle italic
    fontWeight (weight 400)
    marginTop (rem 2)
    marginBottom (cfgContentFontSize doshitanConfig)
    fontSize (rem 1.5)
    lineHeight (unitless 1)

  a ? do
    -- Make bare links same size as when they appear in a <p>
    fontSize inherit
    -- Handle very long links/URLs a little better
    wordWrap breakWord
    overflowWrap breakWord

  (a # hover) ? do
    color linkHoverColor

  input ? do
    "font-family" -: "inherit"
    fontSize (pct 100)
    lineHeight (unitless 1.15)
    sym margin nil

  "[type='button']:not(:disabled), [type='submit']:not(:disabled), button:not(:disabled)" ? do
    cursor pointer

  ".text-button" ? do
    "border" -: "none"
    background transparent
    color inherit
    "font" -: "inherit"

    hover & do
      color linkHoverColor

  "#content" ? do
    sym2 padding (rem 5) nil
    -- remove the Tufte padding from first and last article elements since we do
    -- the padding for page spacing purposes above
    article <? do
      firstChild & do
        paddingTop nil
      lastChild & do
        paddingBottom nil

  ".meta" ? do
    color gray
    "font-variant-caps" -: "small-caps"
    a ? noUnderline

  ".list-inline" ? do
    "list-style" -: "none"
    paddingLeft nil
    sym margin nil
    display flex
    -- undo Tufte style
    "li:not(:first-child)" ? marginTop nil
    li <? do
      position relative
      a <? do
        display inlineBlock
    li |+ li <? paddingLeft (rem 2)

  ".list-inline-sep" ? do
    li |+ li <? do
      before & do
        "content" -: "'•'"
        position absolute
        left (rem 0.7)

  ".list-inline-link-padding" ? do
    li |+ li <? do
      -- undo padding from the li due to .list-inline
      paddingLeft nil
      -- so we can move it to the link itself
      a <? paddingLeft (rem 1)
    li <? do
      a <? do
        paddingTop (rem 0.5)
        paddingBottom (rem 0.5)

  ".list-inline-link-padding-even" ? do
    li |+ li <? do
      -- undo padding from the li due to .list-inline
      paddingLeft nil
    li <? do
      a <? do
        paddingTop (rem 0.5)
        paddingBottom (rem 0.5)
        paddingLeft (rem 1)
        paddingRight (rem 1)

  -- want a bit more spacing than is provided in tufte-css as currently only use
  -- one <dt> element per entry
  "dt:not(:first-child)" ? do
    marginTop (cfgContentFontSize doshitanConfig)

  dd ? do
    marginTop (rem 0.5)
    p # firstChild <? do
      marginTop nil

  -- Adds equivalent padding to what is on the left side of <body>, so that it's
  -- content is roughly centered to the normal page content
  ".container" ? do
    marginRight (pct 14.6)

  smallScreen doshitanConfig $ do
    ".container" ? do
      marginRight nil

  ".post-list" ? do
    "list-style" -: "none"
    paddingLeft nil
    li <? do
      "a" <? do
        noUnderline
        display block
      ".title" ? do
        marginBottom (rem 0.5)
        fontStyle normal
      p ? do
        -- fontSize inherit
        sym margin nil

  ".navbar" ? do
    display flex
    justifyContent spaceBetween
    alignItems baseline
    a ? noUnderline

  ".navbar-brand" ? do
    -- matches the p, ul, ol rule
    fontSize (cfgContentFontSize doshitanConfig)
    sym2 padding (rem 0.5) (rem 1)
    paddingLeft nil

  smallScreen doshitanConfig $ do
    ".navbar-sm-vert" ? do
      flexDirection column

  "#content-info" ? do
    textAlign center
    ul <? li <? a <? do
      -- TODO: could add back padding on the outside edge for the first and last
      -- child
      noUnderline

  smallScreen doshitanConfig $ do
    ".top-link" ? do
      marginTop (rem 1.5)
      alignSelf center

  ".toc-container" ? do
    width (pct 55) -- should match the section > p size

  ".toc" ? do
    float floatRight
    marginLeft (rem 1)
    marginBottom (rem 1)
    maxWidth (pct 33)

    ul ? do
      "list-style" -: "none"
      paddingLeft (rem 1)
      fontSize (rem 1.3)

    ul <? do
      paddingLeft nil

    a ? noUnderline

  ".toc-section-number" ? do
    display none

  ".toc-container-margin" ? do
    width (pct 55) -- should match the section > p size

    h3 ? do
      float floatRight
      marginRight (pct (-60))
      width (pct 50)

    ".toc" ? do
      maxWidth inherit


  smallScreen doshitanConfig $ do
    ".toc-container" ? do
      width (pct 100)

    ".toc" ? do
      float none
      marginLeft nil
      maxWidth (pct 100)

    ".toc-container-margin" ? do
      width (pct 100)

      h3 ? do
        float none
        after & do
          "content" -: "'[+]'"
          position relative
          verticalAlign vAlignBaseline
          fontSize (rem 1)
          top (rem (-0.5))
          left (rem 0.1)

  codeStyles doshitanConfig doshitanDarkConfig

  kbd ? do -- Inspired by Stack Overflow's styling
    sym2 padding (em 0.1) (em 0.6)
    border (px 1) solid "#CCC"
    backgroundColor "#F7F7F7"
    "box-shadow" -: "0 1px 0 rgba(0, 0, 0, 0.2), 0 0 0 2px #FFF inset"
    sym borderRadius (px 3)
    display inlineBlock
    sym2 margin nil (em 0.1)
    textShadow nil (px 1) nil "#FFF"
    "line-height" -: "1.4"

  -- for responsive images
  img ? height auto

  ".img" ? do
    ".pull-left" & margin nil (rem 1) (rem 1) nil

  -- Utils
  ".justify-content-center" ? justifyContent center
  ".pull-left" ? float floatLeft
  ".pull-right" ? float floatRight
  ".text-center" ? textAlign center
  ".hidden" ? display none

  ".alert" ? do
    sym2 padding (rem 0.75) (rem 1.25)
    marginBottom (rem 1)
    "border" -: "1px solid transparent"
    sym borderRadius (rem 0.25)

  ".alert-info" ? do
    color "#0c5460"
    backgroundColor "#d1ecf1"
    borderColor "#bee5eb"

  ".row" ? do
    display flex
    flexDirection row

  smallScreen doshitanConfig $ do
    ".row" ? do
      flexDirection column

  ".column" ? do
    "flex" -: "1 1 auto"
    width (pct 100)
    display block

  "#search-field" ? do
    width (pct 100)
    "border" -: "none"
    sym borderRadius (px 5)
    sym padding (rem 0.5)
    marginBottom (px 5)
    fontSize (cfgContentFontSize doshitanConfig)

  printStyles

  darkStyles doshitanDarkConfig

smallScreen :: Config -> Css -> Css
smallScreen cfg = query Clay.Media.screen [Clay.Media.maxWidth (cfgSmallScreenBreakpoint cfg)]

iconStyles :: Css
iconStyles = do
  fontFace $ do
    fontFamily ["fontello"] []
    fontFaceSrc
      [ FontFaceSrcUrl (fontPath "woff2") (Just WOFF2)
      , FontFaceSrcUrl (fontPath "woff") (Just WOFF)
      , FontFaceSrcUrl (fontPath "ttf") (Just TrueType)
      , FontFaceSrcUrl (fontPath "eot") (Just EmbeddedOpenType)
      ]
    fontWeight normal
    fontStyle normal

  ("[class^='icon-']" <> "[class*='icon-']") ? before & do
    fontFamily ["fontello"] []
    fontStyle normal
    fontWeight normal
    "speak" -: "none"

    display inlineBlock
    textDecoration inherit
    width (em 1)
    marginRight (em 0.2)
    textAlign center

    -- For safety - reset parent styles, that can break glyph codes
    fontVariant normal
    textTransform none

    lineHeight (em 1)

   -- Animation center compensation - margins should be symmetric
   -- remove if not needed
    marginLeft (em 0.2)

    "-webkit-font-smoothing" -: "antialiased"
    "-moz-osx-font-smoothing" -: "grayscale"
  where
    fontPath ext = mconcat
      [ "/assets/font/fontello."
      , ext
      ]

-- Adapted https://github.com/twbs/bootstrap/blob/master/scss/_modal.scss
modalStyles :: Css
modalStyles = do
  ".modal" ? do
    position fixed
    left nil
    top nil
    width (pct 100)
    height (pct 100)
    overflow hidden
    "outline" -: "0"
    zIndex 1

    transition "top" (sec 0.4) easeInOut (sec 0)

    top (pct $ -50)
    opacity 0
    visibility hidden

    ".show" & do
      top nil
      opacity 1
      visibility visible

      ".modal-backdrop" ? do
        opacity 0.5

  ".modal-dialog" ? do
    position relative
    width auto
    sym2 margin (rem 1.75) auto
    maxWidth (px 500)

  ".modal-content" ? do
    -- TODO: should pass the config
    position relative
    display flex
    flexDirection column
    backgroundColor (cfgBackgroundColor doshitanConfig)
    sym borderRadius (px 5)
    "outline" -: "0"
    zIndex 1050

    ".modal-body" ? do
      sym padding (rem 1.75)

  ".modal-backdrop" ? do
    position fixed
    top nil
    left nil
    zIndex 1040
    width (vw 100)
    height (vh 100)
    backgroundColor "#000"
    opacity 0

  ".modal-open" ? do
    overflow hidden

    ".modal" ? do
      overflowX hidden
      overflowY auto

    --TODO: small screen styles, basically nearly 100% the thing

-- https://github.com/edwardtufte/tufte-css/commit/81c72af1cb21cd74104271c076d98a208a6d0c75#diff-f18a581ae9dc15a8a2b31debc6480349L151
-- https://www.smashingmagazine.com/2011/11/how-to-set-up-a-print-style-sheet/
-- https://www.smashingmagazine.com/2015/01/designing-for-print-with-css/
printStyles :: Css
printStyles = query Clay.Media.print [] $ do
  "*, *:before, *:after" ? do
    important $ background transparent
    important $ color "#000"
    important $ "box-shadow" -: "none"
    important $ "text-shadow" -: "none"

  html ? do
    fontSize (px 10)

  body ? do
    width (pct 100)
    paddingLeft nil
    maxWidth none

  -- TODO: the .navbar also catches the last updated and tags at bottom of post,
  -- which is intentional, but maybe those should stick around in print?
  (".navbar" <> "#content-info > .list-inline") ? do
    display none

  "#content" ? sym padding nil

  h1 ? marginTop nil

  sconcat [h1, h2, h3, h4, h5] ? do
    "page-break-after" -: "avoid"

  sconcat [p, h2, h3, h4, h5] ? do
    "orphans" -: "3"
    "widows" -: "3"

  thead ? display tableHeaderGroup

  (table <> tr <> figure <> img) ? do
    "page-break-inside" -: "avoid"

  img ? do important $ maxWidth (pct 100)

  pre ? whiteSpace preWrap

  -- TODO: I'd like to collect links as footnotes at the bottom of a page
  -- (others have done this before, like
  -- https://alistapart.com/article/improvingprint) and there's even the `float:
  -- footnote` attribute we could try, but that's going to require a script to
  -- wrap the urls in some HTML so we can style it properly. For now, we'll just
  -- use the messy way.
  "a[href]:after" ? do
    "content" -: "\" (\" attr(href) \")\""

  -- TODO: would be sweet if we can append the page number for these internal
  -- links, like
  -- https://www.smashingmagazine.com/2015/01/designing-for-print-with-css/#cross-references
  "a[href^=\"#\"]:after" ? do
    display none


codeStyles :: Config -> Config -> Css
codeStyles lightCfg darkCfg = do
  pandocCodeStyles

  "code" ? do
    color (cfgTextColor lightCfg)
    backgroundColor (cfgCodeBackgroundColor lightCfg)
    sym2 padding (px 1) (px 4)
    sym borderRadius (px 3)

  a ? do
    "code" ? do
      "text-shadow" -: "none"
      -- TODO: how should code inside links look?
      -- background inherit

  pre ? do
    boxSizing borderBox
    backgroundColor (cfgCodeBackgroundColor lightCfg)
    sym padding (em 1)
    sym borderRadius (px 3)

    "code" <? do
      width (pct 100)
      marginLeft nil
      sym padding nil

  "pre.numberSource" ? do
    borderTopLeftRadius nil nil
    borderBottomLeftRadius nil nil

  pre # ".no-style" ? do
    backgroundColor transparent
    sym padding nil

  div # ".sourceCode" ? do
    -- https://github.com/jgm/pandoc/issues/673#issuecomment-23091826
    before & do
      "content" -: "attr(data-caption)"
      fontWeight bold
      -- TODO: make this font size stuff better, like applied universally and
      -- actually cascade
      --
      -- to be the same size as a <p>
      fontSize (cfgCodeFontSize lightCfg)
      fontFamily (cfgFontFamilyMono lightCfg) [monospace]

    pre ? do
      overflowX (other "initial")

  -- Code blocks with a language or linenumbers specified get wrapped in a
  -- div.sourceCode, but the properties, like the language class and .fullwidth,
  -- only get applied to the inner pre
  (div # ".sourceCode" <> section) |> pre ? do
    width (pct 55)

    ".fullwidth" & do
      width (pct 100)

  "pre.numberLines > code" ? do
    "margin-left" -: "0"
    "display" -: "inline"

  "pre > code.sourceCode > span > a:first-child::before" ? do
    important $ textDecoration none

  "pre > code.sourceCode > span > a:first-child" ? do
    noUnderline

  code ? do
    "font-variant-numeric" -: "slashed-zero";

  smallScreen doshitanConfig $ do
    (div # ".sourceCode" <> section) |> pre ? do
      width (pct 100)

    "pre.fullwidth > code" ? do
      width (pct 100)

    ".fullwidth" ? do
      maxWidth (pct 100)
    clear both

  codeThemeLightStyles lightCfg

  query Clay.Media.screen [Clay.Stylesheet.Feature "prefers-color-scheme" (Just "dark")] $ do
    "code" ? do
      color (cfgTextColor darkCfg)
      backgroundColor (cfgCodeBackgroundColor darkCfg)

    pre ? do
      backgroundColor (cfgCodeBackgroundColor darkCfg)

    codeThemeDarkStyles darkCfg

pandocCodeStyles :: Css
pandocCodeStyles = do
  -- Pandoc setup
  "pre > code.sourceCode" ? do
    whiteSpace Clay.Text.pre
    position relative

  "pre > code.sourceCode > span" ? do
    display inlineBlock
    lineHeight (unitless 1.25)

  "pre > code.sourceCode > span:empty" ? do
    height (em 1.2)

  "code.sourceCode > span" ? do
    color inherit
    textDecoration inherit

  "div.sourceCode" ? do
    sym2 margin (em 1) nil

  "pre.sourceCode" ? do
    sym margin nil

  query Clay.Media.screen [] $ do
    "div.sourceCode" ? do
      overflow auto

  query Clay.Media.print [] $ do
    "pre > code.sourceCode" ? do
      whiteSpace preWrap

    "pre > code.sourceCode > span" ? do
      textIndent (indent (em $ -5))
      paddingLeft (em 5)

  "pre.numberSource code" ? do
    "counter-reset" -: "source-line 0"

  "pre.numberSource code > span" ? do
    position relative
    left (em $ -4)
    "counter-increment" -: "source-line"

  "pre.numberSource code > span > a:first-child::before" ? do
    "content" -: "counter(source-line)"
    position relative
    left (em $ -1)
    textAlign (alignSide sideRight)
    verticalAlign vAlignBaseline
    "border" -: "none"
    display inlineBlock
    "-webkit-touch-callout" -: "none"
    "-webkit-user-select" -: "none"
    "-khtml-user-select" -: "none"
    "-moz-user-select" -: "none"
    "-ms-user-select" -: "none"
    "user-select" -: "none"
    sym2 padding nil (px 4)
    width (em 4)
    -- In the pandoc styles, but don't want it here
    -- backgroundColor "#ffffff"
    color "#a0a0a0"

  "pre.numberSource" ? do
    marginLeft (em 3)
    borderLeft (px 1) solid "#a0a0a0"
    paddingLeft (px 4)

  "div.sourceCode" ? do
    color "#1f1c1b"
    -- In the pandoc styles, but don't want it here
    -- backgroundColor "#ffffff"

  query Clay.Media.screen [] $ do
    "pre > code.sourceCode > span > a:first-child::before" ? do
      textDecoration underline

-- Based on the Kate theme
codeThemeLightStyles :: Config -> Css
codeThemeLightStyles cfg = do
  code ? do
    Clay.span ? do
      color "#1f1c1b" -- Normal
      ".al" & do -- Alert
        color "#bf0303"
        backgroundColor "#f7e6e6"
        fontWeight bold
      ".an" & color "#ca60ca" -- Annotation
      ".at" & color "#0057ae" -- Attribute
      ".bn" & color "#b08000" -- BaseN
      ".bu" & do -- BuiltIn
        color "#644a9b"
        fontWeight bold
      ".cf" & do -- ControlFlow
        color "#1f1c1b"
        fontWeight bold
      ".ch" & color "#924c9d" -- Char
      ".cn" & color "#aa5500" -- Constant
      ".co" & color "#898887" -- Comment
      ".cv" & color "#0095ff" -- CommentVar
      ".do" & color "#607880" -- Documentation
      ".dt" & color "#0057ae" -- DataType
      ".dv" & color "#b08000" -- DecVal
      ".er" & do -- Error
        color "#bf0303"
        textDecoration underline
      ".ex" & do -- Extension
        color "#0095ff"
        fontWeight bold
      ".fl" & color "#b08000" -- Float
      ".fu" & color "#644a9b" -- Function
      ".im" & color "#ff5500" -- Import
      ".in" & color "#b08000" -- Information
      ".kw" & do -- Keyword
        color "#1f1c1b"
        fontWeight bold
      ".op" & color "#1f1c1b" -- Operator
      ".ot" & color "#006e28" -- Other
      ".pp" & color "#006e28" -- Preprocessor
      ".re" & do -- RegionMarker
        color "#0057ae"
        backgroundColor "#e0e9f8"
      ".sc" & color "#3daee9" -- SpecialChar
      ".ss" & color "#ff5500" -- SpecialString
      ".st" & color "#bf0303" -- String
      ".va" & color "#0057ae" -- Variable
      ".vs" & color "#bf0303" -- VerbatimString
      ".wa" & color "#bf0303" -- Warning

-- Based on the zenburn theme
codeThemeDarkStyles :: Config -> Css
codeThemeDarkStyles cfg = do
  code ? do
    Clay.span ? do
      color "#cccccc" -- Normal
      ".al" & do -- Alert
        color "#ffcfaf"
      ".an" & do
        color "#7f9f7f" -- Annotation
        fontWeight bold
      ".at" & color "#cccccc" -- Attribute
      ".bn" & color "#dca3a3" -- BaseN
      ".bu" & do -- BuiltIn
        color "#cccccc"
        fontWeight normal
      ".cf" & do -- ControlFlow
        color "#f0dfaf"
      ".ch" & color "#dca3a3" -- Char
      ".cn" & do -- Constant
        color "#dca3a3"
        fontWeight bold
      ".co" & color "#7f9f7f" -- Comment
      ".cv" & do -- CommentVar
        color "#7f9f7f"
        fontWeight bold
      ".do" & color "#7f9f7f" -- Documentation
      ".dt" & color "#dfdfbf" -- DataType
      ".dv" & color "#dcdccc" -- DecVal
      ".er" & do -- Error
        color "#c3bf9f"
      ".ex" & do -- Extension
        color "#cccccc"
        fontWeight normal
      ".fl" & color "#c0bed1" -- Float
      ".fu" & color "#efef8f" -- Function
      ".im" & color "#cccccc" -- Import
      ".in" & do -- Information
        color "#7f9f7f"
        fontWeight bold
      ".kw" & do -- Keyword
        color "#f0dfaf"
        fontWeight normal
      ".op" & color "#f0efd0" -- Operator
      ".ot" & color "#efef8f" -- Other
      ".pp" & color "#ffcfaf" -- Preprocessor
      ".re" & do -- RegionMarker
        color "#cccccc"
        backgroundColor inherit
      ".sc" & color "#dca3a3" -- SpecialChar
      ".ss" & color "#cc9393" -- SpecialString
      ".st" & color "#cc9393" -- String
      ".va" & color "#cccccc" -- Variable
      ".vs" & color "#cc9393" -- VerbatimString
      ".wa" & color "#7f9f7f" -- Warning

darkStyles :: Config -> Css
darkStyles cfg = query Clay.Media.screen [Clay.Stylesheet.Feature "prefers-color-scheme" (Just "dark")] $ do
  body ? do
    backgroundColor (cfgBackgroundColor cfg)
    color (cfgTextColor cfg)

  (a # link <> ".tufte-underline" <> ".hover-tufte-underline:hover") ? cfgLinkCss cfg (cfgBackgroundColor cfg)

  ".meta" ? do
    -- TODO: calculate color slightly darker than cfgTextColor?
    color grayLight

  -- TODO: can probably style kbd a bit better than this
  kbd ? do
   "background" -: "unset"
   "box-shadow" -: "none"
   "text-shadow" -: "none"

  -- TODO: alert?

  ".modal-content" ? do
    backgroundColor (cfgBackgroundColor cfg)
