// SERVICE WORKER --------------------------------------------------------------

if ('serviceWorker' in navigator) {
    // Use the window load event to keep the page load performant
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('/service-worker.js');
    });
}

// SEARCH APP ------------------------------------------------------------------

var searchAppConfig =
    { searchIdxUrl: "/search/index.json"
    , searchDataUrl: "/search/data.json"
    , modalId: "site-search-modal"
    }

var searchApp = Elm.SearchMain.init({
    node: document.getElementById('site-search-box'),
    flags: searchAppConfig
});

var searchButton = document.getElementById('site-search-button')
searchButton.href = '#' // remove link to ddg search
searchButton.addEventListener('click', function () {
    searchApp.ports.toggleVisibility.send()
})

var searchIdx
searchApp.ports.loadIndex.subscribe(function (data) {
    searchIdx = lunr.Index.load(JSON.parse(data))
})

searchApp.ports.performSearch.subscribe(function (q) {
    searchApp.ports.searchResults.send(searchIdx.search(q))
});

searchApp.ports.modalOpen.subscribe(function () {
    document.body.classList.add('modal-open')
});


searchApp.ports.modalClose.subscribe(function () {
    document.body.classList.remove('modal-open')
    // currently the only thing that triggers the search, so move focus back to
    // it, this last focus stuff could be better (i.e., don't assume)
    searchButton.focus()
});

document.addEventListener('focus', function (event) {
    var dialog = document.getElementById(searchAppConfig.modalId)

    if (document.body.classList.contains('modal-open') && dialog && !dialog.contains(event.target)) {
        event.stopPropagation()
        dialog.focus()
    }
}, true)
