---
title: Giving
toc: true
published: 2019-04-16T11:23:06-05:00
modified: 2020-09-24T22:19:41-04:00
---

I'm an advocate for [[https://www.effectivealtruism.org/][Effective Altruism]] and I encourage you to read some of the
material on it's site, I won't reproduce it here. I think most people can get
behind the general idea of not giving to /feel good/, but giving to actually /do
good/ (the most possible).

And not all organizations are equal in this regard. There are many worthy causes
and well intentioned people, but to maximize the impact of your dollar, charity
guides, like [[https://www.charitynavigator.org/][Charity Navigator]] and [[https://www.charitywatch.org/][Charity Watch]], are helpful to compare
options. It's your money, and give wherever you believe in, but I'd recommend
giving the majority of your dollars to vetted and effective organizations.

If you are wondering how much to give, there's no absolute answer. Take care of
yourself and your own first. The number doesn't matter too much, even $5/month
to a single organization you think does good work is meaningful. And if you shop
at Amazon, be sure to setup and use [[https://smile.amazon.com/about][Amazon Smile]], costs you nothing.

Giving is [[https://www.givingwhatwecan.org/get-involved/myths-about-aid/][worth doing]]. A general list of [[https://www.charitywatch.org/top-rated-charities][top charities]] might spur your mind.

Note that I am American and my recommendations are biased towards American orgs
or the American branch of orgs. Also this page is currently focused on giving
money, giving your time is another thing to consider.

* General

These are things most people will probably find worth helping.

- [[https://www.doctorswithoutborders.org/][Médecins Sans Frontières/Doctors Without Borders]] (MSF)
- [[https://www.ripmedicaldebt.org/][RIP Medical Debt]]
- [[https://www.givewell.org/][GiveWell]]
- [[https://www.eff.org/][Electronic Frontier Foundation]] (EFF)
- [[https://app.effectivealtruism.org/funds][Effective Altruism Funds]] (some overlap with GiveWell and ACE in certain areas)
- [[https://www.aclu.org/][American Civil Liberties Union]] (ACLU) and your local branch
- [[https://www.feedingamerica.org/find-your-local-foodbank][Your local food bank]]
- Support media creators you like, news sources (such as your local NPR
  station), etc., although you may technically get something back for this,
  think about things you can be a "patron" for and help out just because you
  want it to continue to exist.

* Personal

These are areas I find important, but understand others may not agree.

- [[https://animalcharityevaluators.org/][Animal Charity Evaluators]] (ACE)
- [[https://archive.org/donate/][Internet Archive]]
- [[https://public.resource.org/about/donate.html][Public Resource]]
- Your local no-kill animal shelter
  - Wichita: [[http://lifelineanimalplacement.org][Lifeline Animal Placement and Protection]] (LAPP)
- League of Women Voters and/or your local branch
- Misc. technology things
  - My local dev group
  - [[https://www.patreon.com/bcachefs/overview][bcacefs]]
  - [[https://www.patreon.com/pippin/overview][GIMP]]
  - [[https://signal.org/donate/][Signal]]
  - [[https://wiki.haskell.org/Donate_to_Haskell.org][Haskell.org]]
  - [[https://fund.blender.org/][Blender Development Fund]]
