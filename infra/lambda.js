'use strict';

// Parts taken from:
// https://blog.crashtest-security.com/lambda-edge-to-configure-http-security-headers-for-cloudfront-34a44775061d
exports.handler = async (event, ctx) => {
    // https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/lambda-event-structure.html#lambda-event-structure-response
    const request = event.Records[0].cf.request;
    const response = event.Records[0].cf.response;
    const headers = response.headers;

    // the headers have to be in this weird list of object format for CloudFront
    // https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/lambda-examples.html
    const headersToAdd = [
        [{
            'key': 'Strict-Transport-Security',
            'value': 'max-age=31536000'
        }],
        [{
            'key': 'X-Frame-Options',
            'value': 'deny'
        }],
        [{
            'key': 'X-XSS-Protection',
            'value': '1; mode=block'
        }],
        [{
            'key': 'X-Content-Type-Options',
            'value': 'nosniff'
        }],
        [{
            'key': 'Referrer-Policy',
            'value': 'strict-origin-when-cross-origin'
        }]
    ];

    // this file needs to readable by whatever random client I want to connect with
    if (request.uri === '/.well-known/matrix/client') {
        headersToAdd.push([{
            'key': 'Access-Control-Allow-Origin',
            'value': '*'
        }])
    }

    // add all headers of the array to the response object in the correct format
    for (let header of headersToAdd) {
        headers[header[0].key.toLowerCase()] = header;
    }

    return response;
};
