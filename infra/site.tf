variable "app_name" {}
variable "tld" {}
variable "domain" {}
variable "profile" {}
variable "region" {}
variable "src_bucket" {}
variable "files_bucket" {}

provider "aws" {
  alias = "us-east-1"

  region  = "us-east-1"
  profile = var.profile
}

provider "aws" {
  alias = "main"

  region  = var.region
  profile = var.profile
}

# Content

resource "aws_s3_bucket" "site" {
  provider = aws.main

  bucket = var.src_bucket

  tags = {
    "project" = var.domain
  }
}

resource "aws_s3_bucket_acl" "site" {
  bucket = aws_s3_bucket.site.id

  acl = "public-read"
}

resource "aws_s3_bucket_policy" "site" {
  bucket = aws_s3_bucket.site.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PublicReadForGetBucketObjects",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${var.src_bucket}/*"
    }
  ]
}
POLICY
}

resource "aws_s3_bucket_website_configuration" "site" {
  bucket = aws_s3_bucket.site.id

  index_document {
    suffix = "index.html"
  }
}

resource "aws_s3_bucket_cors_configuration" "site" {
  bucket = aws_s3_bucket.site.id

  # only needed for preloaded fonts
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = ["https://doshitan.com"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3600
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "site" {
  bucket = aws_s3_bucket.site.id

  rule {
    bucket_key_enabled = false

    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket" "www" {
  provider = aws.main

  bucket = "www.${var.domain}"

  tags = {
    "project" = var.domain
  }
}

resource "aws_s3_bucket_acl" "www" {
  bucket = aws_s3_bucket.www.id

  acl = "public-read"
}

resource "aws_s3_bucket_website_configuration" "www" {
  bucket = aws_s3_bucket.www.id

  redirect_all_requests_to {
    host_name = var.domain
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "www" {
  bucket = aws_s3_bucket.www.id

  rule {
    bucket_key_enabled = false

    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_cloudfront_origin_access_identity" "files_oai" {
  provider = aws.main

  comment = "CloudFront OAI for files bucket"
}

resource "aws_s3_bucket" "files" {
  provider = aws.main

  bucket = var.files_bucket

  tags = {
    "project" = var.domain
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "files" {
  bucket = aws_s3_bucket.files.id

  rule {
    bucket_key_enabled = false

    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

data "aws_iam_policy_document" "files_policy" {
  provider = aws.main

  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.files.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.files_oai.iam_arn]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.files.arn]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.files_oai.iam_arn]
    }
  }
}


resource "aws_s3_bucket_policy" "files" {
  provider = aws.main

  bucket = var.files_bucket
  policy = data.aws_iam_policy_document.files_policy.json
}

# TLS

resource "aws_acm_certificate" "domain" {
  provider = aws.us-east-1

  domain_name               = var.domain
  subject_alternative_names = ["*.${var.domain}"]
  validation_method         = "DNS"

  tags = {
    "project" = var.domain
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "acm_validation" {
  provider = aws.us-east-1

  for_each = {
    for dvo in aws_acm_certificate.domain.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = aws_route53_zone.tld.zone_id
}

resource "aws_acm_certificate_validation" "domain" {
  provider = aws.us-east-1

  certificate_arn         = aws_acm_certificate.domain.arn
  validation_record_fqdns = [for record in aws_route53_record.acm_validation : record.fqdn]
}

# CDN

resource "aws_cloudfront_distribution" "site_distribution" {
  provider = aws.main

  origin {
    domain_name = aws_s3_bucket_website_configuration.site.website_endpoint
    origin_id   = aws_s3_bucket.site.id

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  http_version        = "http2"
  default_root_object = "index.html"
  aliases             = [var.domain]
  price_class         = "PriceClass_100"
  retain_on_delete    = true

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = aws_s3_bucket.site.id

    forwarded_values {
      query_string = false
      headers      = ["Origin"]

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 31536000
    max_ttl                = 31536000
    compress               = true

    lambda_function_association {
      event_type = "origin-response"
      lambda_arn = aws_lambda_function.cloudfront.qualified_arn
    }
  }

  custom_error_response {
    error_code         = 400
    response_code      = 400
    response_page_path = "/400.html"
  }

  custom_error_response {
    error_code         = 403
    response_code      = 403
    response_page_path = "/403.html"
  }

  custom_error_response {
    error_code         = 404
    response_code      = 404
    response_page_path = "/404.html"
  }

  custom_error_response {
    error_code         = 405
    response_code      = 405
    response_page_path = "/405.html"
  }

  custom_error_response {
    error_code         = 500
    response_code      = 500
    response_page_path = "/500.html"
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.domain.arn

    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2019"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    "project" = var.domain
  }
}

resource "aws_cloudfront_distribution" "files_distribution" {
  provider = aws.main

  origin {
    domain_name = aws_s3_bucket.files.bucket_domain_name
    origin_id   = aws_s3_bucket.files.id

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.files_oai.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  http_version        = "http2"
  default_root_object = "index.html"
  aliases             = ["files.${var.domain}"]
  price_class         = "PriceClass_100"
  retain_on_delete    = true

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_s3_bucket.files.id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
    compress               = true
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.domain.arn

    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2019"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    "project" = var.domain
  }
}

# DNS

resource "aws_route53_zone" "tld" {
  provider = aws.main

  name = var.tld

  tags = {
    "project" = var.domain
  }
}

resource "aws_route53_record" "caa" {
  provider = aws.main

  zone_id = aws_route53_zone.tld.zone_id
  name    = var.domain
  type    = "CAA"
  ttl     = "300"

  # https://sslmate.com/caa/ is helpful
  records = [
    "0 issue \"amazon.com\"",
    "0 issue \"letsencrypt.org\""
  ]
}

resource "aws_route53_record" "root_v4" {
  provider = aws.main

  zone_id = aws_route53_zone.tld.zone_id
  name    = var.domain
  type    = "A"

  alias {
    name    = aws_cloudfront_distribution.site_distribution.domain_name
    zone_id = aws_cloudfront_distribution.site_distribution.hosted_zone_id

    evaluate_target_health = false
  }
}

resource "aws_route53_record" "root_v6" {
  provider = aws.main

  zone_id = aws_route53_zone.tld.zone_id
  name    = var.domain
  type    = "AAAA"

  alias {
    name    = aws_cloudfront_distribution.site_distribution.domain_name
    zone_id = aws_cloudfront_distribution.site_distribution.hosted_zone_id

    evaluate_target_health = false
  }
}

resource "aws_route53_record" "www_v4" {
  provider = aws.main

  zone_id = aws_route53_zone.tld.zone_id
  name    = "www.${var.domain}"
  type    = "A"

  alias {
    name    = aws_s3_bucket_website_configuration.www.website_domain
    zone_id = aws_s3_bucket.www.hosted_zone_id

    evaluate_target_health = false
  }
}

resource "aws_route53_record" "www_v6" {
  provider = aws.main

  zone_id = aws_route53_zone.tld.zone_id
  name    = "www.${var.domain}"
  type    = "AAAA"

  alias {
    name    = aws_s3_bucket_website_configuration.www.website_domain
    zone_id = aws_s3_bucket.www.hosted_zone_id

    evaluate_target_health = false
  }
}

resource "aws_route53_record" "files_v4" {
  provider = aws.main

  zone_id = aws_route53_zone.tld.zone_id
  name    = "files.${var.domain}"
  type    = "A"

  alias {
    name    = aws_cloudfront_distribution.files_distribution.domain_name
    zone_id = aws_cloudfront_distribution.files_distribution.hosted_zone_id

    evaluate_target_health = false
  }
}

resource "aws_route53_record" "files_v6" {
  provider = aws.main

  zone_id = aws_route53_zone.tld.zone_id
  name    = "files.${var.domain}"
  type    = "AAAA"

  alias {
    name    = aws_cloudfront_distribution.files_distribution.domain_name
    zone_id = aws_cloudfront_distribution.files_distribution.hosted_zone_id

    evaluate_target_health = false
  }
}

# Lambda

data "archive_file" "cloudfront" {
  type        = "zip"
  output_path = "${path.module}/.zip/cloudfront.zip"

  source {
    filename = "lambda.js"
    content  = file("${path.module}/lambda.js")
  }
}

resource "aws_lambda_function" "cloudfront" {
  provider = aws.us-east-1

  function_name    = "${var.app_name}-cloudfront"
  filename         = data.archive_file.cloudfront.output_path
  source_code_hash = data.archive_file.cloudfront.output_base64sha256
  role             = aws_iam_role.cloudfront_lambda.arn
  runtime          = "nodejs14.x"
  handler          = "lambda.handler"
  memory_size      = 128
  timeout          = 3
  publish          = true
}

data "aws_iam_policy_document" "edge_lambda" {
  provider = aws.main

  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"

      identifiers = [
        "lambda.amazonaws.com",
        "edgelambda.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role" "cloudfront_lambda" {
  provider = aws.main

  name_prefix        = var.app_name
  assume_role_policy = data.aws_iam_policy_document.edge_lambda.json
}

resource "aws_iam_role_policy_attachment" "basic" {
  provider = aws.main

  role       = aws_iam_role.cloudfront_lambda.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
